//
//  EditAccountViewController.swift
//  PushNote
//
//  Created by Rizwan on 11/6/14.
//  Copyright (c) 2014 com. All rights reserved.
//

import UIKit
import Alamofire

class EditAccountViewController: BaseViewController, UIAlertViewDelegate {
    
    @IBOutlet weak var textFieldCurrentPasscode: UITextField!
    @IBOutlet weak var textFieldNewPasscode: UITextField!
    @IBOutlet weak var textFieldVerifyPasscode: UITextField!
    @IBOutlet weak var layoutOffsetY: NSLayoutConstraint!
    
    @IBOutlet weak var viewEdit: UIView!
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        textFieldCurrentPasscode.attributedPlaceholder = NSAttributedString(string:"CURRENT PASSCODE",
            attributes:[NSForegroundColorAttributeName: UIColor.whiteColor()])
        textFieldNewPasscode.attributedPlaceholder = NSAttributedString(string:"SET NEW PASSCODE",
            attributes:[NSForegroundColorAttributeName: UIColor.whiteColor()])
        textFieldVerifyPasscode.attributedPlaceholder = NSAttributedString(string:"VERIFY PASSCODE",
            attributes:[NSForegroundColorAttributeName: UIColor.whiteColor()])
        
        var toolbar = UIToolbar(frame: CGRectMake(0, 0, self.view.frame.size.width, 44));
        var itemDone = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.Done, target: self, action: "doneBtnPressed:");
        toolbar.setItems([itemDone], animated: true);
        
        textFieldCurrentPasscode.inputAccessoryView = toolbar;
        textFieldNewPasscode.inputAccessoryView = toolbar;
        textFieldVerifyPasscode.inputAccessoryView = toolbar;
    }
    
    func doneBtnPressed(sender: AnyObject) {
        textFieldCurrentPasscode.resignFirstResponder();
        textFieldNewPasscode.resignFirstResponder();
        textFieldVerifyPasscode.resignFirstResponder();
        
    }
    
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        
        return textField.resignFirstResponder();
    }
    
    func textFieldDidBeginEditing(textField: UITextField) {
        
        if textField == self.textFieldVerifyPasscode {
            if UIScreen.mainScreen().bounds.size.height == 568 {
                self.layoutOffsetY.constant = -20
            }
        }
    }
    
    func textFieldDidEndEditing(textField: UITextField) {
        
        self.layoutOffsetY.constant = 1
    }
    
    @IBAction func deleteNumberBtnPressed(sender: AnyObject) {
        
        var defaults = NSUserDefaults.standardUserDefaults();
        var userNum = defaults.valueForKeyPath("userData.phoneNumber") as String
        
        if userNum.isEmpty {
            var alert :UIAlertView = UIAlertView(title: "", message: "No number is saved", delegate: nil, cancelButtonTitle: "OK")
            alert.show();
        }
        else {
            //UIAlertView(title: "", message: "", delegate: self, cancelButtonTitle: "Cancel", otherButtonTitles: "OK", nil)
            var alert :UIAlertView = UIAlertView(title: "Are you sure", message: "Want to delete your Number?", delegate: self, cancelButtonTitle: "Cancel", otherButtonTitles: "OK")
            alert.show();
        }
        
    }
    
    func alertView(alertView: UIAlertView, clickedButtonAtIndex buttonIndex: Int) {
        
        if buttonIndex == 1 {
            
            if !self.isReachable() {
                return
            }
            
            self.view.userInteractionEnabled = false
            self.activityIndicator.startAnimating()
            
            var defaults = NSUserDefaults.standardUserDefaults();
            var userId = defaults.valueForKeyPath("userData.user_id") as String
            var arrParam = ["userId" : userId]
            
            Alamofire.request(.GET, baseUrl + "deletePhone", parameters:arrParam)
                .responseJSON { (_, _, json, error) -> Void in
                    
                    if error == nil {
                        var jsonResponse: AnyObject = json!
                        if (jsonResponse["status"] as String == "SUCCESS") {
                            
                            var defaults = NSUserDefaults.standardUserDefaults();
                            defaults.setObject(jsonResponse.valueForKey("data"), forKey: "userData");
                            defaults.synchronize();
                            
                            self.navigationController?.popViewControllerAnimated(true)
                        }
                        else {
                            var alert :UIAlertView = UIAlertView(title: "", message: jsonResponse["msg"] as? String, delegate: nil, cancelButtonTitle: "OK")
                            alert.show();
                        }
                    }
                    self.view.userInteractionEnabled = true
                    self.activityIndicator.stopAnimating()
            }

        }
    }

    @IBAction func saveBtnPressed(sender: AnyObject) {
        
        doneBtnPressed(self)
        
        if textFieldCurrentPasscode.text.isEmpty || textFieldNewPasscode.text.isEmpty || textFieldVerifyPasscode.text.isEmpty {
            
            var alert :UIAlertView = UIAlertView(title: "", message: "One or more field is empty", delegate: nil, cancelButtonTitle: "OK")
            alert.show();
        }
        else {
            
            if textFieldNewPasscode.text != textFieldVerifyPasscode.text {
                
                var alert :UIAlertView = UIAlertView(title: "", message: "Passcode mismatch", delegate: nil, cancelButtonTitle: "OK")
                alert.show();
            }
            else {
                
                var defaults = NSUserDefaults.standardUserDefaults();
                var userPass = defaults.valueForKeyPath("userData.password") as String
                
                if textFieldCurrentPasscode.text != userPass {
                    var alert :UIAlertView = UIAlertView(title: "", message: "Current Passcode is Invalid", delegate: nil, cancelButtonTitle: "OK")
                    alert.show();
                }
                else {
                    
                    if !self.isReachable() {
                        return
                    }
                    
                    self.view.userInteractionEnabled = false
                    self.activityIndicator.startAnimating()
                    
                    var userId = defaults.valueForKeyPath("userData.user_id") as String
                    var arrParam = ["userID" : userId]
                    arrParam["passcode"] = textFieldNewPasscode.text

                    Alamofire.request(.GET, baseUrl + "editAccount", parameters:arrParam)
                        .responseJSON { (_, _, json, error) -> Void in
                            
                            if error == nil {
                                var jsonResponse: AnyObject = json!
                                if (jsonResponse["status"] as String == "SUCCESS") {
                                    
                                    var defaults = NSUserDefaults.standardUserDefaults();
                                    defaults.setObject(jsonResponse.valueForKey("data"), forKey: "userData");
                                    defaults.synchronize();

                                    self.navigationController?.popViewControllerAnimated(true)
                                }
                                else {
                                    var alert :UIAlertView = UIAlertView(title: "", message: jsonResponse["msg"] as? String, delegate: nil, cancelButtonTitle: "OK")
                                    alert.show();
                                }
                            }
                            self.view.userInteractionEnabled = true
                            self.activityIndicator.stopAnimating()
                    }
                }
            }
        }
    }
}
