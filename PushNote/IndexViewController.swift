//
//  IndexViewController.swift
//  PushNote
//
//  Created by Rizwan on 11/6/14.
//  Copyright (c) 2014 com. All rights reserved.
//

import UIKit
import Alamofire
import Haneke

class IndexViewController: BaseViewController, UICollectionViewDataSource, UICollectionViewDelegate, UIAlertViewDelegate {
    
    //@IBOutlet weak var _collectionView: UICollectionView!
    var _collectionView: UICollectionView!
    @IBOutlet weak var _tableView: UITableView!
    
    var arrCategory: Array<NSDictionary> = []
    var arrCategoryDetail: NSArray = []
    var isInvite: Bool = false
    var isSubscribe: Bool = false
    var isOpenWebController: Bool = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let layout: UICollectionViewFlowLayout = UICollectionViewFlowLayout()
        layout.sectionInset = UIEdgeInsets(top: 2, left: 2, bottom: 0, right: 0)
        layout.minimumInteritemSpacing = 1.0
        layout.minimumLineSpacing = 1.0
        layout.scrollDirection = UICollectionViewScrollDirection.Horizontal
        layout.itemSize = CGSize(width: 130, height: 130)
        self._collectionView = UICollectionView(frame: CGRect(x: 0, y: 64, width: self.view.frame.size.width, height: 134), collectionViewLayout: layout)
        self._collectionView!.dataSource = self
        self._collectionView!.delegate = self
        self._collectionView!.registerClass(CollectionCell.self, forCellWithReuseIdentifier: "CollectionCell")
        self._collectionView!.backgroundColor = .clearColor()
        self.view.addSubview(self._collectionView!)
        
        self.getCategories()

    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        
    }
    
    func getCategories() {
        
        if !self.isReachable() {
            return
        }

        self.activityIndicator.startAnimating()
        Alamofire.request(.GET, baseUrl + "viewCategory", parameters:nil)
            .responseJSON { (_, _, json, error) -> Void in
                
                if error == nil {
                    var jsonResponse: AnyObject = json!
                    println(jsonResponse)
                    if (jsonResponse["status"] as String == "SUCCESS") {
                        
                        self.arrCategory = jsonResponse["data"] as Array<NSDictionary>
                        
                        self._tableView.reloadData()
                        self._collectionView.reloadData()
                    }
                }
                self.activityIndicator.stopAnimating()
        }

    }
    
    func getSubscribeIndex() {
        
        if !self.isReachable() {
            return
        }
        
        var defaults = NSUserDefaults.standardUserDefaults();
        var userId = defaults.valueForKeyPath("userData.user_id") as String

        self.view.userInteractionEnabled = false
        self.activityIndicator.startAnimating()
        Alamofire.request(.GET, baseUrl + "getMySubscription", parameters:["userId": userId])
            .responseJSON { (_, _, json, error) -> Void in
                
                if error == nil {
                    var jsonResponse: AnyObject = json!
                    println(jsonResponse)
                    if (jsonResponse["status"] as String == "SUCCESS") {
                        
                        self.isInvite = true
                        var tempArr: Array<NSDictionary> = jsonResponse["data"] as Array<NSDictionary>
                        
                        var arr: NSMutableArray = NSMutableArray()
                        
                        for aDic: NSDictionary in tempArr {
                            var newsFeed: NewsFeed = NewsFeed();
                            
                            if aDic["my_feed_id"] != nil {
                                newsFeed.feedId     = aDic["my_feed_id"] as String
                            }
                            if aDic["categoryID"] != nil {
                                newsFeed.categoryId = aDic["categoryID"] as String
                            }
                            if aDic["link"] != nil {
                                newsFeed.link       = aDic["link"] as String
                            }
                            if aDic["title"] != nil {
                                newsFeed.title      = aDic["title"] as String
                            }
                            if aDic["details"] != nil {
                                newsFeed.detail     = aDic["details"] as String
                            }
                            if aDic["user_id"] != nil {
                                newsFeed.userId     = aDic["user_id"] as String
                            }
                            if aDic["feed_password"] != nil {
                                newsFeed.feedPassword  = aDic["feed_password"] as String
                            }
                            if aDic["privacy"] != nil {
                                newsFeed.privacyType  = aDic["privacy"] as String
                            }
                            newsFeed.isSubscribe  = true
                            
                            arr.addObject(newsFeed)
                        }
                        self.arrCategoryDetail = arr
                        self._tableView.reloadSections(NSIndexSet(index: 0), withRowAnimation: UITableViewRowAnimation.Middle)
                    }
                    else {
                        var str: String = jsonResponse["msg"] as String
                        var alert :UIAlertView = UIAlertView(title: "", message: str, delegate: nil, cancelButtonTitle: "OK")
                        alert.show()
                        
                    }
                }
                self.view.userInteractionEnabled = true
                self.activityIndicator.stopAnimating()
        }
        
    }

    
    // MARK: UICollectionView Delegates
    
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        return self.arrCategory.count + 2;
    }
    
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        
        var cell: CollectionCell = collectionView.dequeueReusableCellWithReuseIdentifier("CollectionCell", forIndexPath: indexPath) as CollectionCell;
 
        switch indexPath.row {
        case 0:
            cell.imageView.image = UIImage(named: "subscribed-index.png");
            cell.lblTitle.text = ""
            break
        case 1:
            cell.imageView.image = UIImage(named: "categories.png");
            cell.lblTitle.text = ""
        default :
            let catName = self.arrCategory[indexPath.row-2] as NSDictionary
            cell.imageView.image = nil
            //cell.imageView.hnk_setImageFromURL(NSURL(string: catName["categoryImage"] as String)!)
            cell.lblTitle.text = catName["categoryName"] as String!
            cell.backgroundColor = .blackColor()
        }
        cell.layer.borderWidth = 1.0
        cell.layer.borderColor = UIColor.whiteColor().CGColor
        return cell;
    }
    
    func collectionView(collectionView: UICollectionView, didSelectItemAtIndexPath indexPath: NSIndexPath) {
        
        if indexPath.row == 0 {
            getSubscribeIndex()
        }
        else if indexPath.row == 1 && self.isInvite {
            self.isInvite = false
            
            if self.arrCategory.isEmpty {
                getCategories()
            }
            //getMySubscription, userId
            _tableView.reloadSections(NSIndexSet(index: 0), withRowAnimation: UITableViewRowAnimation.Middle)
        }
        else if indexPath.row > 1 {
            getNewsListforIndex(indexPath.row-2);
        }
    }
    
    // MARK: UITableView Delegates

    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return self.isInvite ? self.arrCategoryDetail.count : self.arrCategory.count;
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        var cell: UITableViewCell
        
        if (self.isInvite) {
            cell = tableView.dequeueReusableCellWithIdentifier("Cell2", forIndexPath: indexPath) as UITableViewCell;
            
            var imageViewBg: UIImageView = cell.contentView.viewWithTag(1000) as UIImageView
            imageViewBg.image = UIImage(named: "bg-find-friend-" + String(indexPath.row%4+1));
            
            let catDetail: NewsFeed = self.arrCategoryDetail[indexPath.row] as NewsFeed
            
            var lblName: UILabel = cell.contentView.viewWithTag(1001) as UILabel
            lblName.text = catDetail.title
            
            var lblDesc: UILabel = cell.contentView.viewWithTag(1002) as UILabel
            lblDesc.text = catDetail.detail
            
            var btnPlus: UIButton = cell.contentView.viewWithTag(1003) as UIButton
            btnPlus.selected = catDetail.isSubscribe
            
            var imageViewLock: UIImageView = cell.contentView.viewWithTag(1004) as UIImageView
            imageViewLock.hidden = true
            if !catDetail.isSubscribe {
                if catDetail.privacyType == "Private" {
                    imageViewLock.hidden = false
                }
            }
        }
        else {
            cell = tableView.dequeueReusableCellWithIdentifier("Cell1", forIndexPath: indexPath) as UITableViewCell;
            
            var lblName: UILabel = cell.contentView.viewWithTag(1001) as UILabel
            
            switch (indexPath.row%6) {
            case 0:
                lblName.backgroundColor = UIColor(red: 239.0/255.0, green: 90.0/255.0, blue: 41.0/255.0, alpha: 1.0)
                break
            case 1:
                lblName.backgroundColor = UIColor(red: 24.0/255.0, green: 187.0/255.0, blue: 197.0/255.0, alpha: 1.0)
                break
            case 2:
                lblName.backgroundColor = UIColor(red: 213.0/255.0, green: 222.0/255.0, blue: 34.0/255.0, alpha: 1.0)
                break
            case 3:
                lblName.backgroundColor = UIColor(red: 221.0/255.0, green: 118.0/255.0, blue: 40.0/255.0, alpha: 1.0)
                break
            case 4:
                lblName.backgroundColor = UIColor(red: 66.0/255.0, green: 85.0/255.0, blue: 99.0/255.0, alpha: 1.0)
                break
            case 5:
                lblName.backgroundColor = UIColor(red: 27.0/255.0, green: 117.0/255.0, blue: 186.0/255.0, alpha: 1.0)
                break
            default:
                break
            }
            let catName = self.arrCategory[indexPath.row] as NSDictionary
            
            lblName.text = catName["categoryName"] as? String

        }
        
        return cell;
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        
        tableView.deselectRowAtIndexPath(indexPath, animated: true)
        if (!self.isInvite) {

            getNewsListforIndex(indexPath.row)
        }
        else {
            
            let catDetail: NewsFeed = self.arrCategoryDetail[indexPath.row] as NewsFeed
            
            if catDetail.privacyType == "Private" {
                self.isSubscribe = true
                self.isOpenWebController = true
                var alert :UIAlertView = UIAlertView(title: "", message: "Please enter the Password", delegate: self, cancelButtonTitle: "OK")
                alert.alertViewStyle = UIAlertViewStyle.SecureTextInput
                alert.textFieldAtIndex(0)?.keyboardType = UIKeyboardType.NumberPad
                alert.textFieldAtIndex(0)?.becomeFirstResponder()
                alert.tag = indexPath.row
                alert.show();
            }
            else {
                var navController = self.storyboard?.instantiateViewControllerWithIdentifier("NavigationControllerWeb") as UINavigationController
                var webController: WebViewController = navController.viewControllers[0] as WebViewController
                webController.link = catDetail.link
                webController.titleWeb = catDetail.title
                self.presentViewController(navController, animated: true, completion: nil);
            }
        }
    }
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return self.isInvite ? 120 : 60;
    }
    
    func showAlertForIndex(index: NSInteger) {
        var alert = UIAlertController(title: "", message: "Please enter the Password",
            preferredStyle: UIAlertControllerStyle.Alert)
        
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.Cancel, handler: { (alertAction: UIAlertAction!) -> Void in
            var textField: UITextField = alert.textFields?.first as UITextField
            //self.lastEnteredCode = textField.text
            if !textField.text.isEmpty {
                let catDetail: NewsFeed = self.arrCategoryDetail[index] as NewsFeed
                if catDetail.feedPassword == textField.text {
                    if(self.isOpenWebController){
                        var navController = self.storyboard?.instantiateViewControllerWithIdentifier("NavigationControllerWeb") as UINavigationController
                        var webController: WebViewController = navController.viewControllers[0] as WebViewController
                        webController.link = catDetail.link
                        webController.titleWeb = catDetail.title
                        self.presentViewController(navController, animated: true, completion: nil);
                    }
                    else{
                        self.subscribeCategoryAtIndex(index)
                    }
                }
            }
            else {
                var alert :UIAlertView = UIAlertView(title: "", message: "Wrong Password", delegate: self, cancelButtonTitle: "Cancel", otherButtonTitles: "Re-Enter")
                alert.tag = 4321
                alert.show()
            }
            
        }))
        
        alert.addTextFieldWithConfigurationHandler({(textField: UITextField!) in
            textField.keyboardType = UIKeyboardType.NumberPad
            textField.secureTextEntry = true
        })
        self.presentViewController(alert, animated: true, completion: nil)
    }
    
    func getNewsListforIndex(index: Int) {
        
        let catName = self.arrCategory[index] as NSDictionary
        var strcatId: String = catName["categoryID"] as String!
        
        var defaults = NSUserDefaults.standardUserDefaults();
        var userId = defaults.valueForKeyPath("userData.user_id") as String
        
        if !self.isReachable() {
            return
        }
        
        self.view.userInteractionEnabled = false
        self.activityIndicator.startAnimating()
        Alamofire.request(.GET, baseUrl + "viewNewsList", parameters:["categoryID": strcatId, "userId": userId])
            .responseJSON { (_, _, json, error) -> Void in
                
                if error == nil {
                    var jsonResponse: NSDictionary = json as NSDictionary
                    println(jsonResponse)
                    if (jsonResponse["status"] as String == "SUCCESS") {
                        self.isInvite = true
                        var tempArr: Array<NSDictionary> = jsonResponse["data"] as Array<NSDictionary>
                        
                        var arr: NSMutableArray = NSMutableArray()
                        
                        for aDic: NSDictionary in tempArr {
                            var newsFeed: NewsFeed = NewsFeed();
                            
                            if aDic["my_feed_id"] != nil {
                                newsFeed.feedId     = aDic["my_feed_id"] as String
                            }
                            if aDic["categoryID"] != nil {
                                newsFeed.categoryId = aDic["categoryID"] as String
                            }
                            if aDic["link"] != nil {
                                newsFeed.link       = aDic["link"] as String
                            }
                            if aDic["title"] != nil {
                                newsFeed.title      = aDic["title"] as String
                            }
                            if aDic["details"] != nil {
                                newsFeed.detail     = aDic["details"] as String
                            }
                            if aDic["user_id"] != nil {
                                newsFeed.userId     = aDic["user_id"] as String
                            }
                            if aDic["feed_password"] != nil {
                                newsFeed.feedPassword  = aDic["feed_password"] as String
                            }
                            if aDic["privacy"] != nil {
                                newsFeed.privacyType  = aDic["privacy"] as String
                            }
                            if aDic["subscribe"] != nil {
                                newsFeed.isSubscribe  = aDic["subscribe"] as Bool
                            }
                            
                            arr.addObject(newsFeed)
                        }
                        self.arrCategoryDetail = arr
                        self._tableView.reloadSections(NSIndexSet(index: 0), withRowAnimation: UITableViewRowAnimation.Middle)
                        self._collectionView.scrollToItemAtIndexPath(NSIndexPath(forItem: index+2, inSection: 0), atScrollPosition: UICollectionViewScrollPosition.CenteredHorizontally, animated: true);
                        
                    }
                    else {
                        var str: String = jsonResponse["msg"] as String
                        var alert :UIAlertView = UIAlertView(title: "", message: str, delegate: nil, cancelButtonTitle: "OK")
                        alert.show()
                        
                    }
                }
                self.view.userInteractionEnabled = true
                self.activityIndicator.stopAnimating()
        }

    }
    
    @IBAction func subscribeBtnPressed(sender: AnyObject, forEvent event: UIEvent) {
        
        var btn: UIButton = sender as UIButton
        var touch: UITouch = event.allTouches()?.anyObject() as UITouch
        var point: CGPoint = touch.locationInView(_tableView) as CGPoint
        var indexPath: NSIndexPath = _tableView.indexPathForRowAtPoint(point) as NSIndexPath!
        
        self.isOpenWebController = false
        if !self.isReachable() {
            return
        }
        
        let catDetail: NewsFeed = self.arrCategoryDetail[indexPath.row] as NewsFeed

        if (!btn.selected) {

            if catDetail.privacyType == "Private" {
                
                self.isSubscribe = true
                var alert :UIAlertView = UIAlertView(title: "", message: "Please enter the Password", delegate: self, cancelButtonTitle: "OK")
                alert.alertViewStyle = UIAlertViewStyle.SecureTextInput
                alert.textFieldAtIndex(0)?.keyboardType = UIKeyboardType.NumberPad
                alert.textFieldAtIndex(0)?.becomeFirstResponder()
                alert.tag = indexPath.row
                alert.show();
            }
            else {
                subscribeCategoryAtIndex(indexPath.row)
            }
            
        }
        else {
            if catDetail.privacyType == "Private" {
                
                self.isSubscribe = false
                var alert :UIAlertView = UIAlertView(title: "Are you sure", message: "Want to unsubscribe private feed?", delegate: self, cancelButtonTitle: "Cancel", otherButtonTitles: "OK")
                alert.tag = indexPath.row
                alert.show();
                
            }
            else {
                unsubscribeCategoryAtIndex(indexPath.row)
            }

        }
    }
    
    func subscribeCategoryAtIndex(index: Int) {
        
        self.activityIndicator.startAnimating()
        self.view.userInteractionEnabled = false
        
        var defaults = NSUserDefaults.standardUserDefaults();
        var userId = defaults.valueForKeyPath("userData.user_id") as String
        
        let catDetail: NewsFeed = self.arrCategoryDetail[index] as NewsFeed
        var feedId = catDetail.feedId

        Alamofire.request(.GET, baseUrl + "addUserNewsFeed", parameters:["userId": userId, "feedId": feedId])
            .responseJSON { (_, _, json, error) -> Void in
                
                if error == nil {
                    var jsonResponse: AnyObject = json!
                    println(jsonResponse)
                    if (jsonResponse["status"] as String == "SUCCESS") {
                        //btn.selected = !btn.selected
                        var catDetail: NewsFeed = self.arrCategoryDetail[index] as NewsFeed
                        catDetail.isSubscribe = true
                        self._tableView.reloadData()
                        
                        var alert :UIAlertView = UIAlertView(title: "Subscribed!", message: "To unsubscribe, tap on the icon.", delegate: nil, cancelButtonTitle: "OK")
                        alert.show()

                    }
                }
                self.view.userInteractionEnabled = true
                self.activityIndicator.stopAnimating()
        }

    }
    
    func unsubscribeCategoryAtIndex(index: Int) {
        
        self.activityIndicator.startAnimating()
        self.view.userInteractionEnabled = false
        
        var defaults = NSUserDefaults.standardUserDefaults();
        var userId = defaults.valueForKeyPath("userData.user_id") as String
        
        let catDetail: NewsFeed = self.arrCategoryDetail[index] as NewsFeed
        var feedId = catDetail.feedId
        
        Alamofire.request(.GET, baseUrl + "unSubscribe", parameters:["userId": userId, "feedId": feedId])
            .responseJSON { (_, _, json, error) -> Void in
                
                if error == nil {
                    var jsonResponse: AnyObject = json!
                    println(jsonResponse)
                    if (jsonResponse["status"] as String == "SUCCESS") {
                        //btn.selected = !btn.selected
                        var catDetail: NewsFeed = self.arrCategoryDetail[index] as NewsFeed
                        catDetail.isSubscribe = false
                        self._tableView.reloadData()
                    }
                }
                self.view.userInteractionEnabled = true
                self.activityIndicator.stopAnimating()
        }
        
    }
    
    func alertView(alertView: UIAlertView, clickedButtonAtIndex buttonIndex: Int) {
        
        if self.isSubscribe {

            var textField: UITextField = alertView.textFieldAtIndex(0)! as UITextField
            if !textField.text.isEmpty {
                let catDetail: NewsFeed = self.arrCategoryDetail[alertView.tag] as NewsFeed
                if catDetail.feedPassword == textField.text {
                    if(self.isOpenWebController){
                        var navController = self.storyboard?.instantiateViewControllerWithIdentifier("NavigationControllerWeb") as UINavigationController
                        var webController: WebViewController = navController.viewControllers[0] as WebViewController
                        webController.link = catDetail.link
                        webController.titleWeb = catDetail.title
                        self.presentViewController(navController, animated: true, completion: nil);
                    }
                    else{
                        subscribeCategoryAtIndex(alertView.tag)
                    }
                }
                else {
                    var alert :UIAlertView = UIAlertView(title: "", message: "Wrong Password", delegate: nil, cancelButtonTitle: "OK")
                    alert.show()
                }
            }
        }
        else {
            if buttonIndex == 1 {
                unsubscribeCategoryAtIndex(alertView.tag)
            }
        }
    }
}
