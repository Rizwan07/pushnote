//
//  CollectionCell.swift
//  PushNote
//
//  Created by Rizwan on 12/11/14.
//  Copyright (c) 2014 com. All rights reserved.
//

import UIKit

class CollectionCell: UICollectionViewCell {

    var imageView: UIImageView!
    var lblTitle: UILabel!
    
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        imageView = UIImageView(frame: CGRect(x: 0, y: 0, width: 130, height: 130))
        //imageView.image = UIImage(named: "subscribed-index")
        imageView.contentMode = UIViewContentMode.ScaleAspectFill
        imageView.clipsToBounds = true
        contentView.addSubview(imageView)
        
        lblTitle = UILabel(frame: CGRect(x: 0, y: 0, width: 130, height: 130))
        lblTitle.font = UIFont.boldSystemFontOfSize(17)
        lblTitle.numberOfLines = 0
        lblTitle.textAlignment = .Center
        lblTitle.textColor = .whiteColor()
        //lblTitle.text = "hello world hello world"
        contentView.addSubview(lblTitle)
    }
    
}
