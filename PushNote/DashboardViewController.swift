//
//  DashboardViewController.swift
//  PushNote
//
//  Created by Rizwan on 11/6/14.
//  Copyright (c) 2014 com. All rights reserved.
//

import MessageUI
import Alamofire

class DashboardViewController: BaseViewController, MFMailComposeViewControllerDelegate, UIAlertViewDelegate {
    
    var lblPushCount: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        var btnLeft: UIButton = UIButton.buttonWithType(UIButtonType.Custom) as UIButton
        var imgLeft: UIImage! = UIImage(named: "menu-push-icon")
        btnLeft.setBackgroundImage(imgLeft, forState: UIControlState.Normal)
        btnLeft.frame = CGRectMake(0, 0, imgLeft.size.width, imgLeft.size.height)
        btnLeft.addTarget(self, action: "notificationBtnPressed:", forControlEvents: UIControlEvents.TouchUpInside)
        
        lblPushCount = UILabel(frame: CGRect(x: 29, y: 0, width: 21, height: 19))
        lblPushCount.font = UIFont.systemFontOfSize(11)
        lblPushCount.textAlignment = NSTextAlignment.Center
        lblPushCount.textColor = .whiteColor()
        lblPushCount.text = "0"
        btnLeft.addSubview(lblPushCount)
        
        var barBtnLeft: UIBarButtonItem = UIBarButtonItem(customView: btnLeft)
        self.navigationItem.leftBarButtonItem = barBtnLeft
        
        var btnRight: UIButton! = UIButton.buttonWithType(UIButtonType.Custom) as? UIButton
        var imgRight: UIImage! = UIImage(named: "btn-logout")
        btnRight.setImage(imgRight, forState: UIControlState.Normal)
        btnRight.frame = CGRectMake(0, 0, imgRight.size.width, imgRight.size.height)
        btnRight.addTarget(self, action: "logoutBtnPressed:", forControlEvents: UIControlEvents.TouchUpInside)
        
        var barBtnRight: UIBarButtonItem = UIBarButtonItem(customView: btnRight)
        self.navigationItem.rightBarButtonItem = barBtnRight
        
        registerPushNotification()
    }
    
    func registerPushNotification() {
        
        if (UIDevice.currentDevice().systemVersion as NSString).floatValue >= 8.0 {
            
//            var replyAction : UIMutableUserNotificationAction = UIMutableUserNotificationAction()
//            replyAction.identifier = "REPLY_ACTION"
//            replyAction.title = "Yes, I need!"
//            
//            replyAction.activationMode = UIUserNotificationActivationMode.Background
//            replyAction.authenticationRequired = false
//            
//            var replyCategory : UIMutableUserNotificationCategory = UIMutableUserNotificationCategory()
//            replyCategory.identifier = "REPLY_CATEGORY"
//            
//            let replyActions:NSArray = [replyAction]
//            
//            replyCategory.setActions(replyActions, forContext: UIUserNotificationActionContext.Default)
//            replyCategory.setActions(replyActions, forContext: UIUserNotificationActionContext.Minimal)
//            
//            let categories = NSSet(object: replyCategory)
            
            var setting: UIUserNotificationSettings = UIUserNotificationSettings(forTypes: UIUserNotificationType.Alert | UIUserNotificationType.Badge | UIUserNotificationType.Sound, categories: nil)
            UIApplication.sharedApplication().registerUserNotificationSettings(setting)
            UIApplication.sharedApplication().registerForRemoteNotifications()
        }
        else {
            //UIApplication.sharedApplication().registerForRemoteNotificationTypes(types: UIUserNotificationType.Alert | UIUserNotificationType.Badge | UIUserNotificationType.Sound)
        }
        
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        var appDelegate = UIApplication.sharedApplication().delegate as AppDelegate
        if appDelegate.dicNotification != nil {
            if appDelegate.dicNotification["url"] != nil {
                appDelegate.addWebViewWithUrl(appDelegate.dicNotification["url"] as String, title: appDelegate.dicNotification["title"] as? String)
                appDelegate.dicNotification = nil
            }
        }
        setNotificationCount()
    }
    
    func setNotificationCount() {
        
//        if !self.isReachable() {
//            return
//        }
        
        var defaults = NSUserDefaults.standardUserDefaults();
        var userId = defaults.valueForKeyPath("userData.user_id") as String
        
        Alamofire.request(.GET, baseUrl + "pushCount", parameters:["userId" : userId])
            .responseJSON { (_, _, json, error) -> Void in
                
                if error == nil {
                    var jsonResponse: AnyObject = json!
                    
                    if (jsonResponse["status"] as String == "SUCCESS") {
                        println("Success:\(jsonResponse)")
                        self.lblPushCount.text = jsonResponse["totalPush"] as? String
                    }
                }
        }
        
    }
    
    func notificationBtnPressed(sender: AnyObject) {
        var notificationsViewController = self.storyboard?.instantiateViewControllerWithIdentifier("NotificationsViewController") as NotificationsViewController
        self.navigationController?.pushViewController(notificationsViewController, animated: true)
    }
    
    @IBAction func logoutBtnPressed(sender: AnyObject) {
        
        var alert :UIAlertView = UIAlertView(title: "", message: "Are you sure?", delegate: self, cancelButtonTitle: "Cancel", otherButtonTitles: "Logout")
        alert.show();
        
    }
    
    func alertView(alertView: UIAlertView, clickedButtonAtIndex buttonIndex: Int) {
        
        if buttonIndex == 1 {
            
            var defaults = NSUserDefaults.standardUserDefaults();
            var userId = defaults.valueForKeyPath("userData.user_id") as String
            self.view.userInteractionEnabled = false
            
            self.activityIndicator.startAnimating()
            
            var param = ["userId" : userId]
            
            Alamofire.request(.GET, baseUrl + "logout", parameters:param)
                .responseJSON { (_, _, json, error) -> Void in
                    
                    if error == nil {
                        var jsonResponse: AnyObject = json!
                        //println("jsonresponse \(jsonResponse)")
                        if (jsonResponse["status"] as String == "SUCCESS") {
                            
                            defaults.removeObjectForKey("userData")
                            defaults.synchronize()
                            
                            self.dismissViewControllerAnimated(true, completion: { () -> Void in
                                
                            });
                        }
                        else {
                            var alert :UIAlertView = UIAlertView(title: "", message: jsonResponse["msg"] as? String, delegate: nil, cancelButtonTitle: "OK")
                            alert.show();
                        }
                    }
                    self.view.userInteractionEnabled = true
                    self.activityIndicator.stopAnimating()
            }
        }
    }
    
    @IBAction func findFriendBtnPressed(sender: AnyObject) {
        
        var defaults = NSUserDefaults.standardUserDefaults();
        var userId = defaults.valueForKeyPath("userData.phoneNumber") as String
        
        if userId.isEmpty {
            var addNumberViewController = self.storyboard?.instantiateViewControllerWithIdentifier("AddPhoneViewController") as AddPhoneViewController
            self.navigationController?.pushViewController(addNumberViewController, animated: true)

        }
        else {
            var findFriendViewController = self.storyboard?.instantiateViewControllerWithIdentifier("FindFriendsViewController") as FindFriendsViewController
            self.navigationController?.pushViewController(findFriendViewController, animated: true)
        }
    }
    
    @IBAction func feedbackBtnPressed(sender: AnyObject) {
        
    }
    
}
