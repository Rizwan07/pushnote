//
//  UnblockViewController.swift
//  PushNote
//
//  Created by Rizwan on 11/6/14.
//  Copyright (c) 2014 com. All rights reserved.
//

import UIKit
import Alamofire

class UnblockViewController: BaseViewController {
    
    @IBOutlet var _tableView: UITableView!
    var arrBlockFriends: NSArray = []
    
    override func viewDidLoad() {
        
        var defaults = NSUserDefaults.standardUserDefaults();
        var userId = defaults.valueForKeyPath("userData.user_id") as String
        
        if !self.isReachable() {
            return
        }

        self.activityIndicator.startAnimating()
        Alamofire.request(.GET, baseUrl + "viewBlockedFriends", parameters:["userID" : userId])
            .responseJSON { (_, _, json, error) -> Void in
                
                if error == nil {
                    var jsonResponse: AnyObject = json!
                    println(jsonResponse)
                    if (jsonResponse["status"] as String == "SUCCESS") {
                        
                        self.arrBlockFriends = jsonResponse["data"] as Array<NSDictionary>
                        
                        self._tableView.reloadData()
                    }
                    else {
                        var alert :UIAlertView = UIAlertView(title: "", message: jsonResponse["msg"] as? String, delegate: nil, cancelButtonTitle: "OK")
                        alert.show();
                    }
                }
                self.activityIndicator.stopAnimating()
        }
    }
    
    @IBAction func unblockAllBtnPressed(sender: AnyObject) {
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return self.arrBlockFriends.count;
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        var cell: UITableViewCell = tableView.dequeueReusableCellWithIdentifier("Cell", forIndexPath: indexPath) as UITableViewCell;
        
        var lblName: UILabel = cell.contentView.viewWithTag(1000) as UILabel
        lblName.text = self.arrBlockFriends[indexPath.row]["username"] as? String
        
//        var lblDesc: UILabel = cell.contentView.viewWithTag(1001) as UILabel
//        lblDesc.text = "name at index " + "indexPath.row"
        
//        var btnLock: UIButton = cell.contentView.viewWithTag(1002) as UIButton
//        btnLock.addTarget(self, action:"lockBtnPressed:", forControlEvents: UIControlEvents.TouchUpInside)
        
        return cell;
    }
    
    @IBAction func unlockBtnPressed(sender: AnyObject, forEvent event: UIEvent) {
        
        print(sender);
        
        var touch: UITouch = event.allTouches()?.anyObject() as UITouch
        var point = touch.locationInView(_tableView) as CGPoint
        var indexPath: NSIndexPath = _tableView.indexPathForRowAtPoint(point) as NSIndexPath!
        
        var userId = self.arrBlockFriends[indexPath.row]["user_id"] as String
        
        if !self.isReachable() {
            return
        }

        self.activityIndicator.startAnimating()
        Alamofire.request(.GET, baseUrl + "unBlock", parameters:["uid" : userId])
            .responseJSON { (_, _, json, error) -> Void in
                
                if error == nil {
                    var jsonResponse: AnyObject = json!
                    if (jsonResponse["status"] as String == "SUCCESS") {
                        
                        println(jsonResponse);
                        var arrMutable: NSMutableArray = self.arrBlockFriends.mutableCopy() as NSMutableArray
                        arrMutable.removeObjectAtIndex(indexPath.row)
                        
                        self.arrBlockFriends = arrMutable
                        self._tableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: UITableViewRowAnimation.Middle)

                    }
                    else {
                        var alert :UIAlertView = UIAlertView(title: "", message: jsonResponse["msg"] as? String, delegate: nil, cancelButtonTitle: "OK")
                        alert.show();
                    }
                }
                self.activityIndicator.stopAnimating()
        }
    }
}
