//
//  HowItWorksViewController.swift
//  PushNote
//
//  Created by Rizwan on 1/19/15.
//  Copyright (c) 2015 com. All rights reserved.
//

import UIKit

class HowItWorksViewController: BaseViewController, UICollectionViewDelegate, UICollectionViewDataSource {
    
    var _collectionView: UICollectionView!
    @IBOutlet weak var pageControl: UIPageControl!
    @IBOutlet weak var lblDesc: UILabel!
    
    let arrDesc: [String] = ["Received a PushNote",
        "How to open a Pushnote Link",
        "How to send a Hello to a friend",
        "Explore the Pushnote Index and subscribe",
        "Share a Pushnote on Social Media",
        "Push this to your friends"]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        var size: CGSize = self.view.frame.size
        
        let layout: UICollectionViewFlowLayout = UICollectionViewFlowLayout()
        layout.sectionInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        layout.minimumInteritemSpacing = 0.0
        layout.minimumLineSpacing = 0.0
        layout.scrollDirection = UICollectionViewScrollDirection.Horizontal
        layout.itemSize = CGSize(width: size.width, height: size.height)
        self._collectionView = UICollectionView(frame: CGRect(x: 0, y: 0, width: size.width, height: size.height), collectionViewLayout: layout)
//        layout.itemSize = CGSize(width: size.width, height: size.height-64)
//        self._collectionView = UICollectionView(frame: CGRect(x: 0, y: 64, width: size.width, height: size.height-64), collectionViewLayout: layout)
        self._collectionView!.dataSource = self
        self._collectionView!.delegate = self
        self._collectionView!.registerClass(UICollectionViewCell.self, forCellWithReuseIdentifier: "CellIdentifier")
        self._collectionView.pagingEnabled = true
        self._collectionView!.backgroundColor = .clearColor()
        self.view.addSubview(self._collectionView!)
        
        self.lblDesc.text = self.arrDesc[self.pageControl.currentPage]
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        
        self.view.bringSubviewToFront(self.pageControl)
        self.view.bringSubviewToFront(self.lblDesc)
    }
    
    // MARK: UICollectionView Delegates
    
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        return 6;
    }
    
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        
        var cell: UICollectionViewCell = collectionView.dequeueReusableCellWithReuseIdentifier("CellIdentifier", forIndexPath: indexPath) as UICollectionViewCell;
        
        //let imageView: UIImageView = cell.viewWithTag(1000)? as UIImageView
        
        if let imgView: UIImageView = cell.viewWithTag(1000) as? UIImageView {
            
            setImagesAnimationToImage(imgView, index: indexPath.row)
        }
        else {
            var imageView: UIImageView = UIImageView(frame: cell.bounds)
            imageView.contentMode = UIViewContentMode.ScaleToFill
            imageView.tag = 1000
            cell.addSubview(imageView)
            setImagesAnimationToImage(imageView, index: indexPath.row)
        }
        
        return cell;
    }
    
    func collectionView(collectionView: UICollectionView, didSelectItemAtIndexPath indexPath: NSIndexPath) {
        
        println("index \(index)")
    }

    func setImagesAnimationToImage(imageView:UIImageView, index:Int) {
        
        var images: [UIImage] = []
        switch index {
        
        case 0:
            images = [
                UIImage(named: "1-1.png")!,
                UIImage(named: "1-2.png")!,
            ]
            
            break
        case 1:
            images = [
                UIImage(named: "2-1.png")!,
                UIImage(named: "2-2.png")!,
                UIImage(named: "2-3.png")!,
            ]
            
            break
        case 2:
            images = [
                UIImage(named: "3-1.png")!,
                UIImage(named: "3-2.png")!,
                UIImage(named: "3-3.png")!,
                UIImage(named: "3-4.png")!,
            ]
            
            break
        case 3:
            images = [
                UIImage(named: "4-1.png")!,
                UIImage(named: "4-2.png")!,
                UIImage(named: "4-3.png")!,
                UIImage(named: "4-4.png")!,
                UIImage(named: "4-5.png")!,
                UIImage(named: "4-6.png")!,
                UIImage(named: "4-7.png")!
            ]
            
            break
        case 4:
            images = [
                UIImage(named: "5-1.png")!,
                UIImage(named: "5-2.png")!,
                UIImage(named: "5-3.png")!,
                UIImage(named: "5-4.png")!,
            ]
            
            break
        case 5:
            images = [
                UIImage(named: "6-1.png")!,
                UIImage(named: "6-2.png")!,
                UIImage(named: "6-3.png")!,
                UIImage(named: "6-4.png")!,
                UIImage(named: "6-5.png")!,
            ]
            
            break
        default:
            images = [
                UIImage(named: "1-1.png")!,
                UIImage(named: "1-2.png")!,
            ]
            break
        
        }
        
        imageView.image = images[0]
        imageView.animationImages = images
        imageView.animationDuration = NSTimeInterval(images.count)
        imageView.animationRepeatCount = 5
        imageView.startAnimating()
    }
    
    func scrollViewDidScroll(scrollView: UIScrollView) {
        
        var point: CGPoint = self._collectionView.contentOffset
        let index = Int(point.x/self.view.frame.size.width)
        self.pageControl.currentPage = index
        
        self.lblDesc.text = self.arrDesc[index]
    }
    
    @IBAction func pageControlValueChanged(sender: AnyObject) {
        
        self._collectionView.scrollToItemAtIndexPath(NSIndexPath(forItem: self.pageControl.currentPage, inSection: 0), atScrollPosition: UICollectionViewScrollPosition.None, animated: true)
        //println(self.pageControl.currentPage)
    }
}
