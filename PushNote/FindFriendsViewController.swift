//
//  FindFriendsViewController.swift
//  PushNote
//
//  Created by Rizwan on 11/6/14.
//  Copyright (c) 2014 com. All rights reserved.
//

import UIKit
import Alamofire
import AddressBookUI
import MessageUI

extension Array {
    var decompose : (head: T, tail: [T])? {
        return (count > 0) ? (self[0], Array(self[1..<count])) : nil
    }
}

extension String {
    func condenseWhitespace() -> String {
        let components = self.componentsSeparatedByCharactersInSet(NSCharacterSet.whitespaceAndNewlineCharacterSet()).filter({!Swift.isEmpty($0)})
        return "".join(components)
    }
}

class FindFriendsViewController: BaseViewController, MFMessageComposeViewControllerDelegate {
    
    var btnDone: UIButton!
    @IBOutlet var _tableView: UITableView!
    @IBOutlet weak var btnFriends: UIButton!
    @IBOutlet weak var btnInvite: UIButton!
    @IBOutlet weak var textFieldSearch: UITextField!
    @IBOutlet weak var btnSelectAll: UIButton!
    
    var arrFriends: Array<FriendData> = []
    var arrFriendsData: Array<FriendData> = []
    var arrContacts: Array<ContactData> = []
    var arrContactsData: Array<ContactData> = []
    var countryCodes = ["93",
        "355",
        "213",
        "1684",
        "376",
        "244",
        "1264",
        "672",
        "1268",
        "54",
        "374",
        "297",
        "61",
        "43",
        "994",
        "1242",
        "973",
        "880",
        "1246",
        "375",
        "32",
        "501",
        "229",
        "1441",
        "975",
        "591",
        "387",
        "267",
        "55",
        "1284",
        "673",
        "359",
        "226",
        "95",
        "257",
        "855",
        "237",
        "238",
        "1345",
        "236",
        "235",
        "56",
        "86",
        "61",
        "61",
        "57",
        "269",
        "682",
        "506",
        "385",
        "53",
        "357",
        "420",
        "243",
        "45",
        "253",
        "1767",
        "1809",
        "593",
        "20",
        "503",
        "240",
        "291",
        "372",
        "251",
        "500",
        "298",
        "679",
        "358",
        "33",
        "689",
        "241",
        "220",
        "970",
        "995",
        "49",
        "233",
        "350",
        "30",
        "299",
        "1473",
        "1671",
        "502",
        "224",
        "245",
        "592",
        "509",
        "39",
        "504",
        "852",
        "36",
        "354",
        "91",
        "62",
        "98",
        "964",
        "353",
        "44",
        "972",
        "39",
        "225",
        "1876",
        "81",
        "962",
        "7",
        "254",
        "686",
        "381",
        "965",
        "996",
        "856",
        "371",
        "961",
        "266",
        "231",
        "218",
        "423",
        "370",
        "352",
        "853",
        "389",
        "261",
        "265",
        "60",
        "960",
        "223",
        "356",
        "692",
        "222",
        "230",
        "262",
        "52",
        "691",
        "373",
        "377",
        "976",
        "382",
        "1664",
        "212",
        "258",
        "264",
        "674",
        "977",
        "31",
        "599",
        "687",
        "64",
        "505",
        "227",
        "234",
        "683",
        "672",
        "850",
        "1670",
        "47",
        "968",
        "92",
        "680",
        "507",
        "675",
        "595",
        "51",
        "63",
        "870",
        "48",
        "351",
        "1",
        "974",
        "242",
        "40",
        "7",
        "250",
        "590",
        "290",
        "1869",
        "1758",
        "1599",
        "508",
        "1784",
        "685",
        "378",
        "239",
        "966",
        "221",
        "381",
        "248",
        "232",
        "65",
        "421",
        "386",
        "677",
        "252",
        "27",
        "82",
        "34",
        "94",
        "249",
        "597",
        "268",
        "46",
        "41",
        "963",
        "886",
        "992",
        "255",
        "66",
        "670",
        "228",
        "690",
        "676",
        "1868",
        "216",
        "90",
        "993",
        "1649",
        "688",
        "256",
        "380",
        "971",
        "44",
        "598",
        "1340",
        "998",
        "678",
        "58",
        "84",
        "681",
        "970",
        "967",
        "260",
        "263"];
    
    var addressBook: ABAddressBookRef?
    var isSelectAllFriends: Bool = false
    var isSelectAllInvited: Bool = false
    
    var selectedCount: Int = 0
    
    @IBOutlet weak var textFieldBgConstraint: NSLayoutConstraint!
    @IBOutlet weak var textFieldConstraint: NSLayoutConstraint!

    override func viewDidLoad() {
        
//        var btn: UIButton = UIButton.buttonWithType(UIButtonType.Custom) as UIButton
//        var img: UIImage! = UIImage(named: "btn-unblock-all")
//        btn.setImage(img, forState: UIControlState.Normal)
//        btn.frame = CGRectMake(0, 0, img.size.width, img.size.height)
//        //btn.addTarget(self, action: "logoutBtnPressed:", forControlEvents: UIControlEvents.TouchUpInside)
//        
//        var barBtn: UIBarButtonItem = UIBarButtonItem(customView: btn)
//        self.navigationItem.rightBarButtonItem = barBtn

        //getFriends()
        
        self.btnDone = UIButton.buttonWithType(UIButtonType.Custom) as UIButton
        var imgLeft: UIImage! = UIImage(named: "done")
        self.btnDone.setImage(imgLeft, forState: UIControlState.Normal)
        self.btnDone.frame = CGRectMake(0, 0, imgLeft.size.width, imgLeft.size.height)
        self.btnDone.addTarget(self, action: "doneBtnPressed:", forControlEvents: UIControlEvents.TouchUpInside)
        
        var barLeftBtn: UIBarButtonItem = UIBarButtonItem(customView: self.btnDone)
        self.navigationItem.rightBarButtonItem = barLeftBtn

        
        textFieldSearch.addTarget(self, action: "searchTableList", forControlEvents: UIControlEvents.EditingChanged)
        
        self.activityIndicator.startAnimating()
        
        let qualityOfServiceClass = QOS_CLASS_BACKGROUND
        let backgroundQueue = dispatch_get_global_queue(qualityOfServiceClass, 0)
        dispatch_async(backgroundQueue, {
            self.getContacts()
        })
        self.changedTab()
    }
    
    func extractABAddressBookRef(abRef: Unmanaged<ABAddressBookRef>!) -> ABAddressBookRef? {
        if let ab = abRef {
            return Unmanaged<NSObject>.fromOpaque(ab.toOpaque()).takeUnretainedValue()
        }
        return nil
    }
    
    func getContacts() {
        if (ABAddressBookGetAuthorizationStatus() == ABAuthorizationStatus.NotDetermined) {
            //println("requesting access...")
            var errorRef: Unmanaged<CFError>? = nil
            addressBook = extractABAddressBookRef(ABAddressBookCreateWithOptions(nil, &errorRef))
            ABAddressBookRequestAccessWithCompletion(addressBook, { success, error in
                if success {
                    self.getContactNames()
                    self.getFriends()
                }
                else {
                    //println("error")
                    dispatch_async(dispatch_get_main_queue(), {
                        self.showAlert()
                    })
                }
            })
        }
        else if (ABAddressBookGetAuthorizationStatus() == ABAuthorizationStatus.Denied || ABAddressBookGetAuthorizationStatus() == ABAuthorizationStatus.Restricted) {
            //println("access denied")
            dispatch_async(dispatch_get_main_queue(), {
                self.showAlert()
            })
        }
        else if (ABAddressBookGetAuthorizationStatus() == ABAuthorizationStatus.Authorized) {
            //println("access granted")
            self.getContactNames()
            self.getFriends()
        }
    }
    
    func showAlert() {
        
        var alert :UIAlertView = UIAlertView(title: "Access denied", message: "Please goto the Settings > PushNote > Enable Contacts", delegate: nil, cancelButtonTitle: "OK")
        alert.show();
        
        self.arrContacts = []
        self.arrContactsData = []
        self.arrFriends = []
        self.arrFriendsData = []
        self.activityIndicator.stopAnimating()
    }
    
    func getContactNames() {
        var errorRef: Unmanaged<CFError>?
        addressBook = extractABAddressBookRef(ABAddressBookCreateWithOptions(nil, &errorRef))
        var contactList: NSArray = ABAddressBookCopyArrayOfAllPeople(addressBook).takeRetainedValue()
        
        var phone: ABMultiValueRef
        
        var arr: Array<ContactData> = []
        
        for record:ABRecordRef in contactList {
            var contactPerson: ABRecordRef = record
            
            //var compositeName: Unmanaged<CFString> = ABRecordCopyCompositeName(contactPerson)
            if let compositeName = ABRecordCopyCompositeName(contactPerson) {
                
                var contactName: String = compositeName.takeRetainedValue() as String
                var unmanagedPhones = ABRecordCopyValue(contactPerson, kABPersonPhoneProperty)
                let phoneObj: ABMultiValueRef = Unmanaged.fromOpaque(unmanagedPhones.toOpaque()).takeUnretainedValue() as NSObject as ABMultiValueRef
                var contactData = ContactData()
                if ABMultiValueGetCount(phoneObj) != 0 {
                    
                    var index = 0 as CFIndex
                    var unmanagedPhone = ABMultiValueCopyValueAtIndex(phoneObj, index)
                    var phoneNumber:String = Unmanaged.fromOpaque(unmanagedPhone.toOpaque()).takeUnretainedValue() as NSObject as String
                    
                    var number = phoneNumber.stringByReplacingOccurrencesOfString("[\\(\\)\\ \\-]", withString: "", options: NSStringCompareOptions.RegularExpressionSearch, range: nil)
                    //let number = phoneNumber.string
                    //let newString = number.stringByReplacingOccurrencesOfString(" ", withString: "", options: NSStringCompareOptions.LiteralSearch, range: nil)
                    let number1 = number.condenseWhitespace()
                    
                    contactData.contactName = contactName
                    contactData.contactPhone = number1
                    //dic = NSDictionary(objectsAndKeys: contactName, "contactName", number1, "contactPhone")
                }
                else {
                    contactData.contactName = contactName
                    //dic = NSDictionary(object: contactName, forKey: "contactName")
                }
                arr.append(contactData)
            }
        }

        self.arrContacts = sorted(arr, forwards)//arr;
        self.arrContactsData = self.arrContacts
//        self._tableView.reloadData()
    }
    
    func forwards(c1: ContactData, c2: ContactData) -> Bool {
        return c1.contactName < c2.contactName
    }
    
    func qsort(input: [String]) -> [String] {
        if let (pivot, rest) = input.decompose {
            let lesser = rest.filter { $0 < pivot }
            let greater = rest.filter { $0 >= pivot }
            return qsort(lesser) + [pivot] + qsort(greater)
        } else {
            return []
        }
    }
    
    @IBAction func topBtnsPressed(sender: AnyObject) {
        
        if ((btnFriends.selected && sender.tag == 0) || (btnInvite.selected && sender.tag == 1)) {
            return;
        }
        btnFriends.selected = sender.tag == 0;
        btnInvite.selected = sender.tag == 1;
        
        self.textFieldSearch.text = ""
        self.textFieldSearch.resignFirstResponder()
        
        self.arrContactsData = self.arrContacts
        self.arrFriendsData = self.arrFriends
        
        self.changedTab()
        _tableView.reloadData()
    }
    
    func changedTab() {
        
        if (btnFriends.selected) {
            
            self.btnSelectAll.alpha = 0.0
            self.textFieldBgConstraint.constant = 15;
            self.textFieldConstraint.constant = 20;
            self.btnDone.hidden = true
            
            //getFriends()
        }
        else {
            
            self.btnSelectAll.alpha = 1.0
            self.textFieldBgConstraint.constant = 100;
            self.textFieldConstraint.constant = 105;
            self.btnDone.hidden = false
            self.btnDone.enabled = (self.selectedCount != 0)
            //self.arrFriends = []
            //test();
            
            //_tableView.reloadData()
        }

    }
    
    func getFriends() {
        
        if !self.isReachable() {
            return
        }
        
        var defaults = NSUserDefaults.standardUserDefaults();
        var userId = defaults.valueForKeyPath("userData.user_id") as String
        self.view.userInteractionEnabled = false
        self.activityIndicator.startAnimating()
        Alamofire.request(.GET, baseUrl + "viewFriends", parameters:["userID" : userId])
            .responseJSON { (_, _, json, error) -> Void in
                
                if error == nil {
                    var jsonResponse: AnyObject = json!
                    //println(jsonResponse)
                    if (jsonResponse["status"] as String == "SUCCESS") {
                        
                        let arr: NSArray = self.arrContactsData
                        var arrContact = jsonResponse["data"] as Array<NSDictionary>
                        for dic: NSDictionary in arrContact {
                            if var phoneNumber: String = dic["phoneNumber"] as? String {
                                
                                //Ayaz: Remove from number
                                phoneNumber = self.removeCountryCode(phoneNumber)
                                
                                let predicate: NSPredicate = NSPredicate(format: "contactPhone contains[cd] %@", phoneNumber)!
                                let arrResult = arr.filteredArrayUsingPredicate(predicate) as NSArray
                                
                                if arrResult.count > 0 {
                                    var friendData: FriendData = FriendData()
                                    friendData.userId = dic["user_id"] as String
                                    friendData.userName = dic["username"] as String
                                    friendData.contactName = arrResult[0].contactName
                                    friendData.userNumber = arrResult[0].contactPhone
                                    self.arrFriends.append(friendData)
                                }
                            }
                        }
                        self.arrFriendsData = self.arrFriends
                        self._tableView.reloadData()
                    }
                    else {
                        var alert :UIAlertView = UIAlertView(title: "", message: jsonResponse["msg"] as? String, delegate: nil, cancelButtonTitle: "OK")
                        alert.show();
                    }
                }
                self.view.userInteractionEnabled = true
                self.activityIndicator.stopAnimating()
        }

    }
    
    func removeCountryCode(phoneNumber:String) -> String {
        var number:NSString = phoneNumber as NSString
        number = number.stringByReplacingOccurrencesOfString("+", withString: "")
        let predicate: NSPredicate = NSPredicate(format: "%@ BEGINSWITH[cd] self", number)!
        var arr = self.countryCodes as NSArray
        let arrResult = arr.filteredArrayUsingPredicate(predicate) as NSArray
        if arrResult.count > 0 {
            number = number.substringFromIndex((arrResult[0] as NSString).length)
        }
        return number as String;
    }
    
    func doneBtnPressed(sender: AnyObject) {
        
        if (MFMessageComposeViewController.canSendText()) {
            
            var compose: MFMessageComposeViewController = MFMessageComposeViewController()
            compose.messageComposeDelegate = self
            
            var arrRecipients: [String] = []
            
            for cData:ContactData in self.arrContactsData {
                if cData.selected && !cData.contactPhone.isEmpty {
                    if !cData.contactPhone.isEmpty {
                        arrRecipients += [cData.contactPhone]
                    }
                }
            }
            
            if !arrRecipients.isEmpty {
                
                compose.recipients = arrRecipients
                //compose.body = "Hey! Checkout this cool app called 'Pushnote'. Enjoy!!"
                
                let userName = NSUserDefaults.standardUserDefaults().valueForKeyPath("userData.username") as String
                //self.textToShare = "Hey! Checkout this cool app called 'Pushnote'. Enjoy!!"
                let textToShare = "I am inviting you to Pushnote (https://itunes.apple.com/gb/app/push-note/id962393538?mt=8) Push me back! @\(userName)"
                compose.body = textToShare
                self.presentViewController(compose, animated: true, completion: nil)
            }
        }
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return btnInvite.selected ? self.arrContactsData.count : self.arrFriendsData.count;
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        var cell: UITableViewCell = tableView.dequeueReusableCellWithIdentifier("Cell", forIndexPath: indexPath) as UITableViewCell;
        
        var imageViewBg: UIImageView = cell.contentView.viewWithTag(1000) as UIImageView
        imageViewBg.image = UIImage(named: "bg-find-friend-" + String(indexPath.row%4+1));
        
        var lblName: UILabel = cell.contentView.viewWithTag(1001) as UILabel
        var lblDesc: UILabel = cell.contentView.viewWithTag(1002) as UILabel
        var btnLock: UIButton = cell.contentView.viewWithTag(1003) as UIButton
        var btnNoti: UIButton = cell.contentView.viewWithTag(1004) as UIButton
        var btnCheckbox: UIButton = cell.contentView.viewWithTag(1005) as UIButton
        var imageViewbox: UIImageView = cell.contentView.viewWithTag(1006) as UIImageView

        if (btnInvite.selected) {
            let contactData = self.arrContactsData[indexPath.row]
            lblName.text = contactData.contactName
            lblDesc.text = contactData.contactPhone
            btnLock.hidden = true
            btnNoti.hidden = true
            btnCheckbox.selected = contactData.selected
            btnCheckbox.hidden = false
            imageViewbox.hidden = false
        }
        else {
            let friendData: FriendData = self.arrFriendsData[indexPath.row]
            lblName.text = friendData.userName + "\n" + friendData.contactName
            lblDesc.text = friendData.userNumber
            btnLock.hidden = false
            btnNoti.hidden = false
            btnCheckbox.selected = friendData.selected
            btnCheckbox.hidden = true
            imageViewbox.hidden = true
        }
        
        return cell;
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        
        if (self.btnInvite.selected) {
            
            if (MFMessageComposeViewController.canSendText()) {
                
                let compose: MFMessageComposeViewController = MFMessageComposeViewController()
                compose.messageComposeDelegate = self
                
                
                let contactUser = self.arrContactsData[indexPath.row]
                if !contactUser.contactPhone.isEmpty {
                    
                    compose.recipients = [contactUser.contactPhone]
                    
                    let userName = NSUserDefaults.standardUserDefaults().valueForKeyPath("userData.username") as String
                    //self.textToShare = "Hey! Checkout this cool app called 'Pushnote'. Enjoy!!"
                    let textToShare = "I am inviting you to Pushnote (https://itunes.apple.com/gb/app/push-note/id962393538?mt=8) Push me back! @\(userName)"
                    compose.body = textToShare
                    self.presentViewController(compose, animated: true, completion: nil)
                }
                
            }
        }
        
        tableView.deselectRowAtIndexPath(indexPath, animated: false)
    }
    
    func messageComposeViewController(controller: MFMessageComposeViewController!, didFinishWithResult result: MessageComposeResult) {
        
        controller.dismissViewControllerAnimated(true, completion: nil)
    }
    
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        return textField.resignFirstResponder()
    }
    
    func searchTableList() {
        
        self.arrFriendsData.removeAll(keepCapacity: false)
        if textFieldSearch.text.isEmpty {
            
            if (btnInvite.selected) {
                self.arrContactsData = self.arrContacts
            }
            else {
                self.arrFriendsData = self.arrFriends
            }

        }
        else {
            if (btnInvite.selected) {
                var arr: NSArray = self.arrContacts
                var predicate: NSPredicate = NSPredicate(format: "contactName contains[cd] %@", textFieldSearch.text)!
                self.arrContactsData = arr.filteredArrayUsingPredicate(predicate) as Array<ContactData>
            }
            else {
                var arr: NSArray = self.arrFriends
                var predicate: NSPredicate = NSPredicate(format: "userName contains[cd] %@", textFieldSearch.text)!
                self.arrFriendsData = arr.filteredArrayUsingPredicate(predicate) as Array<FriendData>
            }

        }
        
        //self.btnDone.enabled = !self.arrFriendsData.isEmpty
        
        self._tableView.reloadData()
    }
    
    @IBAction func lockBtnPressed(sender: AnyObject, forEvent event: UIEvent) {
        
        if !self.isReachable() {
            return
        }
        
        var touch: UITouch = event.allTouches()?.anyObject() as UITouch
        var point = touch.locationInView(_tableView) as CGPoint
        var indexPath: NSIndexPath = _tableView.indexPathForRowAtPoint(point) as NSIndexPath!
        
        var userId = self.arrFriendsData[indexPath.row].userId
        
        self.activityIndicator.startAnimating()
        Alamofire.request(.GET, baseUrl + "block", parameters:["uid" : userId])
            .responseJSON { (_, _, json, error) -> Void in
                
                if error == nil {
                    var jsonResponse: AnyObject = json!
                    if (jsonResponse["status"] as String == "SUCCESS") {
                        
                        println(jsonResponse);

                        self.arrFriendsData.removeAtIndex(indexPath.row)
                        self._tableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: UITableViewRowAnimation.Middle)
                    }
                    else {
                        var alert :UIAlertView = UIAlertView(title: "", message: jsonResponse["msg"] as? String, delegate: nil, cancelButtonTitle: "OK")
                        alert.show();
                    }
                }
                self.activityIndicator.stopAnimating()
        }
        
    }
    
    @IBAction func pushBtnPressed(sender: AnyObject, forEvent event: UIEvent) {
        
        var touch: UITouch = event.allTouches()?.anyObject() as UITouch
        var point = touch.locationInView(_tableView) as CGPoint
        var indexPath: NSIndexPath = _tableView.indexPathForRowAtPoint(point) as NSIndexPath!
        
        var userId = self.arrFriendsData[indexPath.row].userId

        var defaults = NSUserDefaults.standardUserDefaults();
        var senderName = defaults.valueForKeyPath("userData.username") as String

        self.view.userInteractionEnabled = false
        
        self.activityIndicator.startAnimating()
        Alamofire.request(.GET, baseUrl + "sendPush", parameters:["senderName": senderName, "userId" : userId, "link": ""])
            .responseJSON { (_, _, json, error) -> Void in
                
                if error == nil {
                    var jsonResponse: AnyObject = json!
                    
                    if (jsonResponse["status"] as String == "SUCCESS") {
                        
                        var alert :UIAlertView = UIAlertView(title: "", message: "Push Sent!", delegate: nil, cancelButtonTitle: "OK")
                        alert.show()

//                        if indexPath.row != 0 {
//                            
//                            var contactData = self.arrFriendsData[indexPath.row]
//                            self.arrFriendsData.removeAtIndex(indexPath.row)
//                            self.arrFriendsData.insert(contactData, atIndex: 0)
//                            
//                            self._tableView.moveRowAtIndexPath(indexPath, toIndexPath: NSIndexPath(forRow: 0, inSection: 0))
//                        }
                    }
                    else {
                        var alert :UIAlertView = UIAlertView(title: "", message: jsonResponse["message"] as? String, delegate: nil, cancelButtonTitle: "OK")
                        alert.show();
                    }
                }
                self.view.userInteractionEnabled = true
                self.activityIndicator.stopAnimating()
        }
    }
    
    @IBAction func selectBtnPressed(sender: AnyObject, forEvent event: UIEvent) {
        
        var touch: UITouch = event.allTouches()?.anyObject() as UITouch
        var point: CGPoint = touch.locationInView(self._tableView)  as CGPoint
        var indexPath: NSIndexPath = self._tableView.indexPathForRowAtPoint(point) as NSIndexPath!
        
        var btn: UIButton = sender as UIButton
        
        btn.selected = !btn.selected
        self.arrContactsData[indexPath.row].selected = btn.selected
        selectedCount += btn.selected ? 1 : -1
        self.btnDone.enabled = (selectedCount != 0)
    }
    
    @IBAction func selectAllBtnPressed(sender: AnyObject) {
        
//        self.isSelectAll = !self.isSelectAll
        
        if (btnInvite.selected) {
            self.isSelectAllInvited = !self.isSelectAllInvited
            for cData:ContactData in self.arrContactsData {
                cData.selected = self.isSelectAllInvited
            }
            selectedCount = self.isSelectAllInvited ? self.arrContactsData.count : 0
            self.btnDone.enabled = (selectedCount != 0)
        }
        else {
            self.isSelectAllFriends = !isSelectAllFriends
            for fData:FriendData in self.arrFriendsData {
                fData.selected = self.isSelectAllFriends
            }
        }
        
        self._tableView.reloadData()
    }

}