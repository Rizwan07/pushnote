//
//  AboutTextViewController.swift
//  PushNote
//
//  Created by Rizwan on 1/19/15.
//  Copyright (c) 2015 com. All rights reserved.
//

import UIKit

class AboutTextViewController: BaseViewController {
    
    var myTitle: String = ""
    @IBOutlet weak var webView: UIWebView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationItem.title = self.myTitle.uppercaseString
        
        var path: String

        if self.myTitle == "About" {
            path = NSBundle.mainBundle().pathForResource("about", ofType: "html")!
        }
        else if self.myTitle == "Privacy Policy" {
            path = NSBundle.mainBundle().pathForResource("privacy", ofType: "html")!
        }
        else {
            path = NSBundle.mainBundle().pathForResource("terms", ofType: "html")!
        }
        var request: NSURLRequest = NSURLRequest(URL: NSURL(fileURLWithPath: path)!)
        self.webView.loadRequest(request)
    }
    
}
