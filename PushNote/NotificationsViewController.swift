//
//  NotificationsViewController.swift
//  PushNote
//
//  Created by Rizwan on 1/1/15.
//  Copyright (c) 2015 com. All rights reserved.
//

import UIKit
import Alamofire

class NotificationsViewController: BaseViewController,UITableViewDataSource,UITableViewDelegate, UIAlertViewDelegate {
    
    @IBOutlet weak var _tableView: UITableView!
    
    var arrNotifications: Array<NSDictionary>! = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        var btnRight: UIButton! = UIButton.buttonWithType(UIButtonType.Custom) as? UIButton
        var imgRight: UIImage! = UIImage(named: "clear-all")
        btnRight.setImage(imgRight, forState: UIControlState.Normal)
        btnRight.frame = CGRectMake(0, 0, imgRight.size.width, imgRight.size.height)
        btnRight.addTarget(self, action: "clearAllBtnPressed:", forControlEvents: UIControlEvents.TouchUpInside)
        
        var barBtnRight: UIBarButtonItem = UIBarButtonItem(customView: btnRight)
        self.navigationItem.rightBarButtonItem = barBtnRight
        
        getNotifications()
    }
    
    func clearAllBtnPressed(sender: AnyObject) {
        
        var alert :UIAlertView = UIAlertView(title: "Are you sure?", message: "Want to clear all notifications", delegate: self, cancelButtonTitle: "Cancel", otherButtonTitles: "OK")
        alert.show();
        
    }
    
    func getNotifications() {
        
        var defaults = NSUserDefaults.standardUserDefaults();
        var userId = defaults.valueForKeyPath("userData.user_id") as String
        
        self.activityIndicator.startAnimating()
        //getLocalPush
        Alamofire.request(.GET, baseUrl + "updateNotificationStatus", parameters:["userId" : userId])
            .responseJSON { (_, _, json, error) -> Void in
                
                if error == nil {
                    var jsonResponse: AnyObject = json!
                    
                    if (jsonResponse["status"] as String == "SUCCESS") {
                        self.arrNotifications = jsonResponse["data"] as Array<NSDictionary>
                        self._tableView.reloadData()
                    }
                    else {
                        var alert :UIAlertView = UIAlertView(title: "", message: jsonResponse["msg"] as? String, delegate: nil, cancelButtonTitle: "OK")
                        alert.show();
                    }
                }
                self.activityIndicator.stopAnimating()
        }
        
//                Alamofire.request(.POST, baseUrl + "pushCountZero", parameters:["userId" : userId])
//                    .responseJSON { (_, _, json, error) -> Void in
//                        
//                        if error == nil {
//                            var jsonResponse: AnyObject = json!
//                            println("Success:\(jsonResponse)")
//                            
//                            if (jsonResponse["status"] as String == "SUCCESS") {
//                                
//                            }
//                            else {
//                                var alert :UIAlertView = UIAlertView(title: "", message: jsonResponse["msg"] as? String, delegate: nil, cancelButtonTitle: "OK")
//                                alert.show();
//                            }
//                        }
//        
//                self.activityIndicator.stopAnimating()
//        }
        
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return self.arrNotifications.count;
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        var cell: UITableViewCell = tableView.dequeueReusableCellWithIdentifier("Cell", forIndexPath: indexPath) as UITableViewCell;
        
        var imageViewBg: UIImageView = cell.contentView.viewWithTag(1000) as UIImageView
        imageViewBg.image = UIImage(named: "bg-find-friend-" + String(indexPath.row%4+1));
        
        var lblName: UILabel = cell.contentView.viewWithTag(1001) as UILabel
        var lblDesc: UILabel = cell.contentView.viewWithTag(1002) as UILabel
        
        var txt: String = self.arrNotifications[indexPath.row]["txt"] as String
 
        lblName.text = txt.stringByReplacingOccurrencesOfString("\n", withString: "", options: nil, range: nil)
        
        lblDesc.text = self.arrNotifications[indexPath.row]["timestamp"] as? String
        
        return cell;
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        
        tableView.deselectRowAtIndexPath(indexPath, animated: true)
        
        var dic: NSDictionary = self.arrNotifications[indexPath.row]
        
        var navController = self.storyboard?.instantiateViewControllerWithIdentifier("NavigationControllerWeb") as UINavigationController
        var webController: WebViewController = navController.viewControllers[0] as WebViewController
        webController.link = dic["url"] as? String
        webController.titleWeb = dic["title"] as? String
        self.presentViewController(navController, animated: true, completion: nil);
        
    }
    
    func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
        if (editingStyle == UITableViewCellEditingStyle.Delete) {
            deleteNotification(self.arrNotifications[indexPath.row]["id"] as String, type: "single", index: indexPath.row)
//            self.arrNotifications.removeAtIndex(indexPath.row)
//            tableView.deleteRowsAtIndexPaths(NSArray(object: indexPath), withRowAnimation: UITableViewRowAnimation.Automatic)
        }
    }

    func alertView(alertView: UIAlertView, clickedButtonAtIndex buttonIndex: Int) {
        
        if buttonIndex == 1 {
            println("Clear All")
            deleteNotification("0", type: "all", index: 0)
        }
    }
    
    func deleteNotification(notificationId: String, type: String, index: Int) {
        
        var defaults = NSUserDefaults.standardUserDefaults();
        var userId = defaults.valueForKeyPath("userData.user_id") as String
        
        self.activityIndicator.startAnimating()
        //ttp://loveitbutton.com/services/pushnote/services/index/deleteNotification/?userId=6&type=single&notificationId=47
        Alamofire.request(.GET, baseUrl + "deleteNotification", parameters:["userId" : userId, "type": type, "notificationId": notificationId])
            .responseJSON { (_, _, json, error) -> Void in
                
                if error == nil {
                    var jsonResponse: AnyObject = json!
                    
                    if (jsonResponse["status"] as String == "SUCCESS") {
                        
                        if type == "single" {
                            self.arrNotifications.removeAtIndex(index)
                            self._tableView.deleteRowsAtIndexPaths(NSArray(object: NSIndexPath(forRow: index, inSection: 0)), withRowAnimation: UITableViewRowAnimation.Automatic)
                        }
                        else {
                            self.arrNotifications.removeAll(keepCapacity: false)
                            self._tableView.reloadData()
                        }
                    }
                    else {
                        var alert :UIAlertView = UIAlertView(title: "", message: jsonResponse["msg"] as? String, delegate: nil, cancelButtonTitle: "OK")
                        alert.show();
                    }
                }
                self.activityIndicator.stopAnimating()
        }

    }
    
}
