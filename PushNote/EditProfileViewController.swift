//
//  EditProfileViewController.swift
//  PushNote
//
//  Created by Rizwan on 11/6/14.
//  Copyright (c) 2014 com. All rights reserved.
//

import UIKit
import Alamofire
import Haneke

class EditProfileViewController: BaseViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate, UIActionSheetDelegate {
    
    @IBOutlet weak var imageViewUser: UIImageView!
    @IBOutlet weak var indicatorProfileImage: UIActivityIndicatorView!
    @IBOutlet weak var textFieldUsername: UITextField!
    var isImageChange: Bool = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        textFieldUsername.attributedPlaceholder = NSAttributedString(string:"ENTER USER NAME",
            attributes:[NSForegroundColorAttributeName: UIColor.whiteColor()])
        
        var toolbar = UIToolbar(frame: CGRectMake(0, 0, self.view.frame.size.width, 44));
        var itemDone = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.Done, target: self, action: "doneBtnPressed:")
        toolbar.setItems([itemDone], animated: true)
        
        textFieldUsername.inputAccessoryView = toolbar;
        
        var defaults = NSUserDefaults.standardUserDefaults();
        var userId = defaults.valueForKeyPath("userData.user_id") as String
        var userPhoto = defaults.valueForKeyPath("userData.photo") as String
        var userName = defaults.valueForKeyPath("userData.username") as String
        self.textFieldUsername.text = userName
        
        self.indicatorProfileImage.startAnimating()
        //self.imageViewUser.hnk_setImageFromURL(NSURL(string: userPhoto)!)
        
        let imageFormat = Format<UIImage>(name: "icons", diskCapacity: 10 * 1024 * 1024)

        self.imageViewUser.hnk_setImageFromURL(NSURL(string: userPhoto)!, placeholder: UIImage(named: "enter-user-image"), format: imageFormat, failure: { (error) -> () in
            self.indicatorProfileImage.stopAnimating()
        }) { (image) -> () in
            self.imageViewUser.image = image
            self.indicatorProfileImage.stopAnimating()
        }
    }
    
    func doneBtnPressed(sender: AnyObject) {
        textFieldUsername.resignFirstResponder();
    }

    @IBAction func editImageBtnPressed(sender: AnyObject) {
        
        let actionSheet = UIActionSheet(title: "Picture taken Options", delegate: self, cancelButtonTitle: "Cancel", destructiveButtonTitle: nil, otherButtonTitles: "Take Picture", "Import from Gallery")
        actionSheet.showInView(self.view)
    }
    
    func imagePickerController(picker: UIImagePickerController!, didFinishPickingImage image: UIImage!, editingInfo: [NSObject : AnyObject]!) {
        
        imageViewUser.image = image;
        picker.dismissViewControllerAnimated(true, completion: nil);
        self.isImageChange = true
    }
    
    func imagePickerControllerDidCancel(picker: UIImagePickerController) {
        picker.dismissViewControllerAnimated(true, completion: nil);
    }
    
    func actionSheet(actionSheet: UIActionSheet!, clickedButtonAtIndex buttonIndex: Int)
    {
        switch buttonIndex {
            
        case 0:
            NSLog("Cancel");
            break;
        case 1:
            if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.Camera) {
                
                let imagePickerControler = UIImagePickerController();
                imagePickerControler.delegate = self;
                imagePickerControler.sourceType = UIImagePickerControllerSourceType.Camera;
                imagePickerControler.allowsEditing = true
                self.presentViewController(imagePickerControler, animated: true, completion: nil);
            }

            break;
        case 2:
            if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.PhotoLibrary) {
                
                let imagePickerControler = UIImagePickerController();
                imagePickerControler.delegate = self;
                imagePickerControler.sourceType = UIImagePickerControllerSourceType.PhotoLibrary;
                imagePickerControler.allowsEditing = true
                self.presentViewController(imagePickerControler, animated: true, completion: nil);
            }

            break;
        default:
            NSLog("Default");
            break;
            //Some code here..
            
        }
    }
    
    @IBAction func saveBtnPressed(sender: AnyObject) {
        
        var defaults = NSUserDefaults.standardUserDefaults();
        var userId = defaults.valueForKeyPath("userData.user_id") as String
        
        if !self.isReachable() {
            return
        }
        
        textFieldUsername.resignFirstResponder()
        self.activityIndicator.startAnimating()
        
        var param = ["userID" : userId, "username" : textFieldUsername.text]
        
        if self.isImageChange {
            
            let qualityOfServiceClass = QOS_CLASS_BACKGROUND
            let backgroundQueue = dispatch_get_global_queue(qualityOfServiceClass, 0)
            dispatch_async(backgroundQueue, {
                param["profile_image"] = UIImageJPEGRepresentation(self.imageViewUser.image, 0.4).base64EncodedStringWithOptions(NSDataBase64EncodingOptions.Encoding64CharacterLineLength)
                self.editProfileWithParam(param)
            })
        }
        else {
            self.editProfileWithParam(param)
        }
    }
    
    func editProfileWithParam(param: [String: AnyObject]) {
        
        Alamofire.request(.POST, baseUrl + "editProfile", parameters:param)
            .responseJSON { (_, _, json, error) -> Void in
                
                if error == nil {
                    var jsonResponse: AnyObject = json!
                    if (jsonResponse["status"] as String == "SUCCESS") {
                        
                        println("json edit \(jsonResponse)")
                        
                        var defaults = NSUserDefaults.standardUserDefaults();
                        defaults.setObject(jsonResponse.valueForKey("data"), forKey: "userData");
                        defaults.synchronize();
                        
                        self.navigationController?.popViewControllerAnimated(true)
                    }
                    else {
                        var alert :UIAlertView = UIAlertView(title: "", message: jsonResponse["msg"] as? String, delegate: nil, cancelButtonTitle: "OK")
                        alert.show();
                    }
                }
                self.activityIndicator.stopAnimating()
        }

    }

    //MARK: TextField Delegate
    
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        
        return textField.resignFirstResponder();
    }
    
}