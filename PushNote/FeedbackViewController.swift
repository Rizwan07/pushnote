//
//  FeedbackViewController.swift
//  PushNote
//
//  Created by Rizwan on 1/19/15.
//  Copyright (c) 2015 com. All rights reserved.
//

import UIKit
import MessageUI

class FeedbackViewController: BaseViewController, MFMailComposeViewControllerDelegate {
    
    var arrListNames: [String] = ["Rate", "Feedback"]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        var abc = 23
        var xyz = abc * 7
        println(xyz)
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return self.arrListNames.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        var cell: UITableViewCell = tableView.dequeueReusableCellWithIdentifier("Cell", forIndexPath: indexPath) as UITableViewCell;
        
        var imageViewBg: UIImageView = cell.contentView.viewWithTag(1000) as UIImageView
        imageViewBg.image = UIImage(named: self.arrListNames[indexPath.row]);
        
        var lblName: UILabel = cell.contentView.viewWithTag(1001) as UILabel
        lblName.text = self.arrListNames[indexPath.row]
        
        return cell;
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        
        switch (indexPath.row) {
        case 0:
            //var appStoreId = "962393538"
            //var urlStr: String = "itms-apps://itunes.apple.com/app/id\(appStoreId)"
            var urlStr: String = "http://itunes.apple.com/WebObjects/MZStore.woa/wa/viewContentsUserReviews?id=962393538&pageNumber=0&sortOrdering=2&type=Purple+Software&mt=8"
            UIApplication.sharedApplication().openURL(NSURL(string: urlStr)!)
            
            break
        case 1:
            if (MFMailComposeViewController.canSendMail()) {
                let mailComposer = MFMailComposeViewController();
                mailComposer.mailComposeDelegate = self;
                mailComposer.setToRecipients(["feedback@ipushnote.com"]);
                mailComposer.setSubject("PushNote Feeback");
                //mailComposer.setMessageBody("", isHTML: false);
                self.presentViewController(mailComposer, animated: true, completion: nil);
            }
            
            break
        default:
            break
        }
        
        tableView.deselectRowAtIndexPath(indexPath, animated: true)
    }
    
    func mailComposeController(controller: MFMailComposeViewController!, didFinishWithResult result: MFMailComposeResult, error: NSError!) {
        
        switch (result.value) {
        case MFMailComposeResultSent.value:
            //let alert = UIAlertView.alloc()
            break;
        default:
            break;
        }
        controller.dismissViewControllerAnimated(true, completion: nil);
    }
    
}
