//
//  HomeViewController.swift
//  PushNote
//
//  Created by Rizwan on 11/6/14.
//  Copyright (c) 2014 com. All rights reserved.
//

import UIKit
import Alamofire

class HomeViewController: BaseViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate, UIActionSheetDelegate {
    
    private var expandingIndexPath: NSIndexPath!
    private var expandedIndexPath: NSIndexPath!

    var toolbar: UIToolbar!
    var isImageBrowse: Bool = false
    
    @IBOutlet weak var _tableView: UITableView!
    var textFieldDummy: UITextField!
    
    var tableViewHeaderHeight: CGFloat!

    override func viewDidLoad() {
        
        toolbar = UIToolbar(frame: CGRectMake(0, 0, self.view.frame.size.width, 44));
        var itemDone = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.Done, target: self, action: "doneBtnPressed:")
        toolbar.setItems([itemDone], animated: true)
        
        tableViewHeaderHeight = 60*3 + 120;
        
        _tableView.tableHeaderView = UIView(frame: CGRectMake(0, 0, _tableView.frame.size.width, (_tableView.frame.size.height - (tableViewHeaderHeight))/2))

    }
    
    func doneBtnPressed(sender: AnyObject) {
        textFieldDummy.resignFirstResponder();
    }
    
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        return textField.resignFirstResponder();
    }
    
    func textFieldDidBeginEditing(textField: UITextField) {
        textFieldDummy = textField
//        if (UIScreen.mainScreen().bounds.size.height == 480) {
//            
//        }
//        else {
            var pt: CGPoint
            var rc: CGRect = textField.bounds
            rc = textField.convertRect(rc, toView: _tableView)
            pt = rc.origin
            pt.x = 0;
            if (pt.y > 200) {
                
                pt.y -= 200;
                _tableView.setContentOffset(pt, animated: true)
            }
//        }
    }
    
    func textFieldDidEndEditing(textField: UITextField) {
        _tableView.setContentOffset(CGPointMake(0, 0), animated: true)
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if (self.expandedIndexPath != nil) {
            return 4;
        }
        return 3;
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        var cellIdentifier: NSString = "ExpandingCellIdentifier";
        
        // init expanded cell
        if (indexPath.isEqual(self.expandedIndexPath)) {
            switch (indexPath.row) {
            case 1:
                cellIdentifier = "ExpandedCellIdentifier1";
                break;
            case 2:
                cellIdentifier = "ExpandedCellIdentifier2";
                break;
            case 3:
                cellIdentifier = "ExpandedCellIdentifier3";
                break;
                
            default:
                break;
            }
            
        }
            // init expanding cell
        else {
            cellIdentifier = "ExpandingCellIdentifier";
        }
        
        var cell: UITableViewCell = tableView.dequeueReusableCellWithIdentifier(cellIdentifier, forIndexPath: indexPath) as UITableViewCell;
        
        if cellIdentifier == "ExpandingCellIdentifier" {
            var imageView = cell.contentView.viewWithTag(1000) as UIImageView!
            switch (indexPath.row) {
            case 0:
                imageView.image = UIImage(named: "sign-up.png");
                break;
            case 1:
                imageView.image = UIImage(named: "login.png");
                break;
            case 2:
                imageView.image = UIImage(named: "recover-pass.png");
                break;
                
            default:
                break;
            }
        }
        
//        var imageViewBg: UIImageView = cell.contentView.viewWithTag(1000) as UIImageView
//        imageViewBg.image = UIImage(named: "bg-find-friend-" + String(indexPath.row%4+1));
//        
//        var lblDesc: UILabel = cell.contentView.viewWithTag(1001) as UILabel
//        lblDesc.text = "name at index " + String(indexPath.row)
        
        //        var btnLock: UIButton = cell.contentView.viewWithTag(1002) as UIButton
        //        btnLock.addTarget(self, action:"lockBtnPressed:", forControlEvents: UIControlEvents.TouchUpInside)
        
        return cell;
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        
        // disable touch on expanded cell
        var cell: UITableViewCell = tableView.cellForRowAtIndexPath(indexPath) as UITableViewCell!
        
        if (cell.reuseIdentifier == "ExpandedCellIdentifier1" ||
            cell.reuseIdentifier == "ExpandedCellIdentifier2" ||
            cell.reuseIdentifier == "ExpandedCellIdentifier3") {
            return;
        }

        var mIndexPath = indexPath;
        
        if (self.expandedIndexPath != nil) {
            if indexPath.row > self.expandedIndexPath.row {
                mIndexPath =  NSIndexPath(forRow: indexPath.row - 1, inSection: indexPath.section)
            }
        }
        
        // save the expanded cell to delete it later
        var theExpandedIndexPath: NSIndexPath? = nil
        if self.expandedIndexPath != nil {
            theExpandedIndexPath = self.expandedIndexPath as NSIndexPath?;
        }
        
        // same row tapped twice - get rid of the expanded cell
        if (expandingIndexPath != nil && mIndexPath == self.expandingIndexPath) {
            self.expandingIndexPath = nil;
            self.expandedIndexPath = nil;
        }
            // add the expanded cell
        else {
            self.expandingIndexPath = mIndexPath;
            self.expandedIndexPath = NSIndexPath(forRow: mIndexPath.row+1, inSection: mIndexPath.section);
            //[NSIndexPath indexPathForRow:[indexPath row] + 1 inSection:[indexPath section]];
        }
        
        tableView.beginUpdates();
        
        if (theExpandedIndexPath != nil) {
            _tableView.deleteRowsAtIndexPaths([theExpandedIndexPath!], withRowAnimation: UITableViewRowAnimation.None);
            if self.expandedIndexPath == nil {
                tableView.deselectRowAtIndexPath(indexPath, animated: false)
                
                _tableView.tableHeaderView = UIView(frame: CGRectMake(0, 0, _tableView.frame.size.width, (_tableView.frame.size.height - (tableViewHeaderHeight))/2))

            }
            
        }
        
        if (self.expandedIndexPath != nil) {
            
            var height: CGFloat = 0
            switch (self.expandedIndexPath.row) {
            case 1:
                height = 60*3 + 320
            case 2:
                height = 60*3 + 140;
            case 3:
                height = 60*3 + 120;
            default:
                break;
            }

            _tableView.tableHeaderView = UIView(frame: CGRectMake(0, 0, _tableView.frame.size.width, (_tableView.frame.size.height - height)/2))
            _tableView.insertRowsAtIndexPaths([self.expandedIndexPath!], withRowAnimation: UITableViewRowAnimation.None);
        }
        
        tableView.endUpdates();
        
        
        // scroll to the expanded cell
        self._tableView.scrollToRowAtIndexPath(mIndexPath, atScrollPosition: UITableViewScrollPosition.Middle, animated: true);

    }
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        
        if (indexPath.isEqual(expandedIndexPath)) {
            switch (indexPath.row) {
            case 1:
                return 320;
            case 2:
                return 140;
            case 3:
                return 120;
            default:
                break;
            }
        }
        return 60;
    }
    
    @IBAction func browseImageBtnPressed(sender: AnyObject, forEvent event: UIEvent) {
        
        let actionSheet = UIActionSheet(title: "Picture taken Options", delegate: self, cancelButtonTitle: "Cancel", destructiveButtonTitle: nil, otherButtonTitles: "Take Picture", "Import from Gallery")
        actionSheet.showInView(self.view)

        
//        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.PhotoLibrary) {
//            
//            let imagePickerControler = UIImagePickerController();
//            imagePickerControler.delegate = self;
//            imagePickerControler.sourceType = UIImagePickerControllerSourceType.PhotoLibrary;
//            imagePickerControler.allowsEditing = true
//            self.presentViewController(imagePickerControler, animated: true, completion: nil);
//        }
    }
    func imagePickerController(picker: UIImagePickerController!, didFinishPickingImage image: UIImage!, editingInfo: [NSObject : AnyObject]!) {
        var cell: UITableViewCell = _tableView.cellForRowAtIndexPath(expandedIndexPath) as UITableViewCell!
        var imgView = cell.contentView.viewWithTag(1000) as UIImageView!;
        imgView.image = image;
        picker.dismissViewControllerAnimated(true, completion: nil);
        self.isImageBrowse = true
    }
    func imagePickerControllerDidCancel(picker: UIImagePickerController) {
        picker.dismissViewControllerAnimated(true, completion: nil);
    }
    
    func actionSheet(actionSheet: UIActionSheet!, clickedButtonAtIndex buttonIndex: Int)
    {
        switch buttonIndex {
            
        case 0:
            NSLog("Cancel");
            break;
        case 1:
            if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.Camera) {
                
                let imagePickerControler = UIImagePickerController();
                imagePickerControler.delegate = self;
                imagePickerControler.sourceType = UIImagePickerControllerSourceType.Camera;
                imagePickerControler.allowsEditing = true
                self.presentViewController(imagePickerControler, animated: true, completion: nil);
            }
            
            break;
        case 2:
            if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.PhotoLibrary) {
                
                let imagePickerControler = UIImagePickerController();
                imagePickerControler.delegate = self;
                imagePickerControler.sourceType = UIImagePickerControllerSourceType.PhotoLibrary;
                imagePickerControler.allowsEditing = true
                self.presentViewController(imagePickerControler, animated: true, completion: nil);
            }
            
            break;
        default:
            NSLog("Default");
            break;
            
        }
    }
    
    @IBAction func signupBtnPressed(sender: AnyObject, forEvent event: UIEvent) {
        
        var cell: UITableViewCell = _tableView.cellForRowAtIndexPath(expandedIndexPath) as UITableViewCell!
        var textFieldEmail = cell.contentView.viewWithTag(1001) as UITextField
        var textFieldUsername = cell.contentView.viewWithTag(1002) as UITextField
        var textFieldPasscode = cell.contentView.viewWithTag(1003) as UITextField
        if (textFieldEmail.text.isEmpty || textFieldUsername.text.isEmpty || textFieldPasscode.text.isEmpty) {
            
            var alert :UIAlertView = UIAlertView(title: "", message: "All fields required", delegate: nil, cancelButtonTitle: "OK")
            alert.show()
        }
        else {
            
            if !self.isReachable() {
                return
            }
            
            var param = ["email": textFieldEmail.text, "username": textFieldUsername.text, "password": textFieldPasscode.text]
            
            self.view.userInteractionEnabled = false
            self.textFieldDummy.resignFirstResponder()
            self.activityIndicator.startAnimating()

            if self.isImageBrowse {
                
                var imgView = cell.contentView.viewWithTag(1000) as UIImageView!

                let qualityOfServiceClass = QOS_CLASS_BACKGROUND
                let backgroundQueue = dispatch_get_global_queue(qualityOfServiceClass, 0)
                dispatch_async(backgroundQueue, {
                    param["profile_image"] = UIImageJPEGRepresentation(imgView.image, 0.4).base64EncodedStringWithOptions(NSDataBase64EncodingOptions.Encoding64CharacterLineLength)
                    self.signupWithParam(param)
                    
//                    dispatch_async(dispatch_get_main_queue(), 0), {
//                        println("This is run on the main queue, after the previous code in outer block")
//                    })
                })
                
            }
            else {
                self.signupWithParam(param)
            }
        }
    }
    
    func signupWithParam(param: [String: AnyObject]) {
        
        Alamofire.request(.POST, baseUrl + "signup", parameters: param)
            .responseJSON { (_, _, json, error) -> Void in
                if error == nil {
                    var jsonResponse: NSDictionary = json! as NSDictionary
                    println("json response \(jsonResponse)")
                    if (jsonResponse["status"] as String == "SUCCESS") {
                        
                        var defaults = NSUserDefaults.standardUserDefaults();
                        defaults.setObject(jsonResponse.valueForKey("data"), forKey: "userData");
                        defaults.synchronize();
                        
                        self.dismissViewControllerAnimated(true, completion: nil)
                    }
                    else {
                        var str: String = jsonResponse["msg"] as String
                        var alert :UIAlertView = UIAlertView(title: "", message: str, delegate: nil, cancelButtonTitle: "OK")
                        alert.show()
                        
                    }
                }
                self.view.userInteractionEnabled = true
                self.activityIndicator.stopAnimating()
        }

    }
    
    @IBAction func loginBtnPressed(sender: AnyObject, forEvent event: UIEvent) {
        
        //ttp://ipushnote.com/pushnote/services/index/login?username=fahad&password=123
        
        var cell: UITableViewCell = _tableView.cellForRowAtIndexPath(expandedIndexPath) as UITableViewCell!
        var textFieldUsername = cell.contentView.viewWithTag(1001) as UITextField
        var textFieldPasscode = cell.contentView.viewWithTag(1002) as UITextField

        if (textFieldUsername.text.isEmpty || textFieldPasscode.text.isEmpty) {
            
            var alert :UIAlertView = UIAlertView(title: "", message: "Both fields required", delegate: nil, cancelButtonTitle: "OK")
            alert.show()
        }
        else {
            
            if !self.isReachable() {
                return
            }
            self.textFieldDummy.resignFirstResponder()
            self.activityIndicator.startAnimating()
            Alamofire.request(.GET, baseUrl + "login", parameters: ["username": textFieldUsername.text, "password": textFieldPasscode.text])
                .responseJSON { (_, _, json, error) -> Void in
                    //let jsonResponse = JSON(json: json)
                    if error == nil {
                        let jsonResponse: NSDictionary = json! as NSDictionary
                        println("json \(jsonResponse)");
                        if (jsonResponse["status"] as String == "SUCCESS") {
                            
                            var defaults = NSUserDefaults.standardUserDefaults();
                            defaults.setObject(jsonResponse.valueForKey("data"), forKey: "userData");
                            defaults.synchronize();
                            
                            self.dismissViewControllerAnimated(true, completion: nil)
                            
    //                        var navController = self.storyboard?.instantiateViewControllerWithIdentifier("NavC") as UINavigationController
    //                        self.presentViewController(navController, animated: true, completion: { () -> Void in
    //                            textFieldUsername.text = ""
    //                            textFieldPasscode.text = ""
    //                        });
                        }
                        else {
                            var str: String = jsonResponse["msg"] as String
                            var alert :UIAlertView = UIAlertView(title: "", message: str, delegate: nil, cancelButtonTitle: "OK")
                            alert.show()
                        }
                    }
                    self.activityIndicator.stopAnimating()
                }
    
        
//            .response { (request, response, data, error) in
//                println(request)
//                println(response)
//                println(error)
//        }
        }
    }
    
    @IBAction func forgotPasscodeBtnPressed(sender: AnyObject, forEvent event: UIEvent) {
        
        var cell: UITableViewCell = _tableView.cellForRowAtIndexPath(expandedIndexPath) as UITableViewCell!
        var textFieldUsername = cell.contentView.viewWithTag(1001) as UITextField
        if (textFieldUsername.text.isEmpty) {
            
            var alert :UIAlertView = UIAlertView(title: "", message: "Username is required", delegate: nil, cancelButtonTitle: "OK")
            alert.show()
        }
        else {
            
            if !self.isReachable() {
                return
            }
            self.textFieldDummy.resignFirstResponder()
            self.activityIndicator.startAnimating()
            Alamofire.request(.GET, baseUrl + "forgotpassword", parameters:["username": textFieldUsername.text])
                .responseJSON { (_, _, json, error) -> Void in
                    
                    if error == nil {
                        var jsonResponse: NSDictionary = json! as NSDictionary
                        if (jsonResponse["status"] as String == "SUCCESS") {
                            
                            //var str: String = jsonResponse["msg"] as String
                            var alert :UIAlertView = UIAlertView(title: "", message: jsonResponse["msg"] as? String, delegate: nil, cancelButtonTitle: "OK")
                            alert.show();
                            textFieldUsername.text = ""
                        }
                    }
                    self.activityIndicator.stopAnimating()
            }
        }
    }
    
}