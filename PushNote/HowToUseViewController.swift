//
//  HowToUseViewController.swift
//  PushNote
//
//  Created by Rizwan on 11/6/14.
//  Copyright (c) 2014 com. All rights reserved.
//

import UIKit

class HowToUseViewController: BaseViewController {
    
    @IBOutlet weak var _scrollView: UIScrollView!
    @IBOutlet weak var _pageControl: UIPageControl!
    
    var arrListImages: Array<String>! = []
    var arrListNames: Array<String>! = []
    
    override func viewDidLoad() {
        
        self.navigationItem.title = "ABOUT"
        
        self.arrListImages = ["setting_about", "setting_work", "setting_privacy-policy", "setting_terms-condition"]
        self.arrListNames = ["About", "How It Works", "Privacy Policy", "Terms & Conditions"]
        
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return self.arrListImages.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        var cell: UITableViewCell = tableView.dequeueReusableCellWithIdentifier("Cell", forIndexPath: indexPath) as UITableViewCell;
        
        var imageViewBg: UIImageView = cell.contentView.viewWithTag(1000) as UIImageView
        imageViewBg.image = UIImage(named: self.arrListImages[indexPath.row]);
        
        var lblName: UILabel = cell.contentView.viewWithTag(1001) as UILabel
        lblName.text = self.arrListNames[indexPath.row]
        
        return cell;
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        
        switch (indexPath.row) {
        case 0:
            var aboutTextViewController = self.storyboard?.instantiateViewControllerWithIdentifier("AboutTextViewController") as AboutTextViewController
            aboutTextViewController.myTitle = self.arrListNames[indexPath.row]
            self.navigationController?.pushViewController(aboutTextViewController, animated: true)
            
            break
        case 1:
            var howItWorksViewController = self.storyboard?.instantiateViewControllerWithIdentifier("HowItWorksViewController") as HowItWorksViewController
            self.navigationController?.pushViewController(howItWorksViewController, animated: true)

            break
        case 2:
            var aboutTextViewController = self.storyboard?.instantiateViewControllerWithIdentifier("AboutTextViewController") as AboutTextViewController
            aboutTextViewController.myTitle = self.arrListNames[indexPath.row]
            self.navigationController?.pushViewController(aboutTextViewController, animated: true)

            break
        case 3:
            var aboutTextViewController = self.storyboard?.instantiateViewControllerWithIdentifier("AboutTextViewController") as AboutTextViewController
            aboutTextViewController.myTitle = self.arrListNames[indexPath.row]
            self.navigationController?.pushViewController(aboutTextViewController, animated: true)

            break
        default:
            break
        }
        
        tableView.deselectRowAtIndexPath(indexPath, animated: true)
    }
    
}
