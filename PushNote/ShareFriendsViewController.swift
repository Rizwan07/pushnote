//
//  ShareFriendsViewController.swift
//  PushNote
//
//  Created by Rizwan on 12/23/14.
//  Copyright (c) 2014 com. All rights reserved.
//

import UIKit
import Alamofire
import AddressBookUI

class ShareFriendsViewController: BaseViewController {
    
    @IBOutlet weak var textFieldSearch: UITextField!
    @IBOutlet weak var _tableView: UITableView!
    
    var btnDone: UIButton!
    
    var arrFriends: Array<FriendData> = []
    var arrFriendsData: Array<FriendData> = []
    
    var arrContacts: Array<NSDictionary> = []
    var countryCodes = ["93",
        "355",
        "213",
        "1684",
        "376",
        "244",
        "1264",
        "672",
        "1268",
        "54",
        "374",
        "297",
        "61",
        "43",
        "994",
        "1242",
        "973",
        "880",
        "1246",
        "375",
        "32",
        "501",
        "229",
        "1441",
        "975",
        "591",
        "387",
        "267",
        "55",
        "1284",
        "673",
        "359",
        "226",
        "95",
        "257",
        "855",
        "237",
        "238",
        "1345",
        "236",
        "235",
        "56",
        "86",
        "61",
        "61",
        "57",
        "269",
        "682",
        "506",
        "385",
        "53",
        "357",
        "420",
        "243",
        "45",
        "253",
        "1767",
        "1809",
        "593",
        "20",
        "503",
        "240",
        "291",
        "372",
        "251",
        "500",
        "298",
        "679",
        "358",
        "33",
        "689",
        "241",
        "220",
        "970",
        "995",
        "49",
        "233",
        "350",
        "30",
        "299",
        "1473",
        "1671",
        "502",
        "224",
        "245",
        "592",
        "509",
        "39",
        "504",
        "852",
        "36",
        "354",
        "91",
        "62",
        "98",
        "964",
        "353",
        "44",
        "972",
        "39",
        "225",
        "1876",
        "81",
        "962",
        "7",
        "254",
        "686",
        "381",
        "965",
        "996",
        "856",
        "371",
        "961",
        "266",
        "231",
        "218",
        "423",
        "370",
        "352",
        "853",
        "389",
        "261",
        "265",
        "60",
        "960",
        "223",
        "356",
        "692",
        "222",
        "230",
        "262",
        "52",
        "691",
        "373",
        "377",
        "976",
        "382",
        "1664",
        "212",
        "258",
        "264",
        "674",
        "977",
        "31",
        "599",
        "687",
        "64",
        "505",
        "227",
        "234",
        "683",
        "672",
        "850",
        "1670",
        "47",
        "968",
        "92",
        "680",
        "507",
        "675",
        "595",
        "51",
        "63",
        "870",
        "48",
        "351",
        "1",
        "974",
        "242",
        "40",
        "7",
        "250",
        "590",
        "290",
        "1869",
        "1758",
        "1599",
        "508",
        "1784",
        "685",
        "378",
        "239",
        "966",
        "221",
        "381",
        "248",
        "232",
        "65",
        "421",
        "386",
        "677",
        "252",
        "27",
        "82",
        "34",
        "94",
        "249",
        "597",
        "268",
        "46",
        "41",
        "963",
        "886",
        "992",
        "255",
        "66",
        "670",
        "228",
        "690",
        "676",
        "1868",
        "216",
        "90",
        "993",
        "1649",
        "688",
        "256",
        "380",
        "971",
        "44",
        "598",
        "1340",
        "998",
        "678",
        "58",
        "84",
        "681",
        "970",
        "967",
        "260",
        "263"];

    var addressBook: ABAddressBookRef?

    var link: String!
    var titleWeb: String!
    var isSelectedAll: Bool = false
    var userMessage: NSString!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.btnDone = UIButton.buttonWithType(UIButtonType.Custom) as UIButton
        var imgLeft: UIImage! = UIImage(named: "done")
        self.btnDone.setImage(imgLeft, forState: UIControlState.Normal)
        self.btnDone.frame = CGRectMake(0, 0, imgLeft.size.width, imgLeft.size.height)
        self.btnDone.addTarget(self, action: "doneBtnPressed:", forControlEvents: UIControlEvents.TouchUpInside)
        
        var barLeftBtn: UIBarButtonItem = UIBarButtonItem(customView: self.btnDone)
        self.navigationItem.rightBarButtonItem = barLeftBtn
        
        textFieldSearch.addTarget(self, action: "searchTableList", forControlEvents: UIControlEvents.EditingChanged)
        
        //getFriends()
        self.activityIndicator.startAnimating()
        
        let qualityOfServiceClass = QOS_CLASS_BACKGROUND
        let backgroundQueue = dispatch_get_global_queue(qualityOfServiceClass, 0)
        dispatch_async(backgroundQueue, {
            self.getContacts()
        })

    }
    
    func extractABAddressBookRef(abRef: Unmanaged<ABAddressBookRef>!) -> ABAddressBookRef? {
        if let ab = abRef {
            return Unmanaged<NSObject>.fromOpaque(ab.toOpaque()).takeUnretainedValue()
        }
        return nil
    }
    
    func getContacts() {
        if (ABAddressBookGetAuthorizationStatus() == ABAuthorizationStatus.NotDetermined) {
            //println("requesting access...")
            var errorRef: Unmanaged<CFError>? = nil
            addressBook = extractABAddressBookRef(ABAddressBookCreateWithOptions(nil, &errorRef))
            ABAddressBookRequestAccessWithCompletion(addressBook, { success, error in
                if success {
                    self.getContactNames()
                    self.getFriends()
                }
                else {
                    //println("error")
                    dispatch_async(dispatch_get_main_queue(), {
                        self.showAlert()
                    })
                }
            })
        }
        else if (ABAddressBookGetAuthorizationStatus() == ABAuthorizationStatus.Denied || ABAddressBookGetAuthorizationStatus() == ABAuthorizationStatus.Restricted) {
            //println("access denied")
            dispatch_async(dispatch_get_main_queue(), {
                self.showAlert()
            })
        }
        else if (ABAddressBookGetAuthorizationStatus() == ABAuthorizationStatus.Authorized) {
            //println("access granted")
            self.getContactNames()
            self.getFriends()
        }
    }
    
    func showAlert() {
        
        var alert :UIAlertView = UIAlertView(title: "Access denied", message: "Please goto the Settings > PushNote > Enable Contacts", delegate: nil, cancelButtonTitle: "OK")
        alert.show();
        
        self.arrContacts = []
        self.arrFriends = []
        self.activityIndicator.stopAnimating()
    }
    
    func getContactNames() {
        var errorRef: Unmanaged<CFError>?
        addressBook = extractABAddressBookRef(ABAddressBookCreateWithOptions(nil, &errorRef))
        var contactList: NSArray = ABAddressBookCopyArrayOfAllPeople(addressBook).takeRetainedValue()
        
        var phone: ABMultiValueRef
        
        var arr: Array<NSDictionary> = []
        
        for record:ABRecordRef in contactList {
            var contactPerson: ABRecordRef = record
            
            //var compositeName: Unmanaged<CFString> = ABRecordCopyCompositeName(contactPerson)
            if let compositeName = ABRecordCopyCompositeName(contactPerson) {
                
                var contactName: String = compositeName.takeRetainedValue() as String
                var unmanagedPhones = ABRecordCopyValue(contactPerson, kABPersonPhoneProperty)
                let phoneObj: ABMultiValueRef = Unmanaged.fromOpaque(unmanagedPhones.toOpaque()).takeUnretainedValue() as NSObject as ABMultiValueRef
                var dic: NSDictionary
                if ABMultiValueGetCount(phoneObj) != 0 {
                    
                    var index = 0 as CFIndex
                    var unmanagedPhone = ABMultiValueCopyValueAtIndex(phoneObj, index)
                    var phoneNumber:String = Unmanaged.fromOpaque(unmanagedPhone.toOpaque()).takeUnretainedValue() as NSObject as String
                    
                    var number = phoneNumber.stringByReplacingOccurrencesOfString("[\\(\\)\\ \\-]", withString: "", options: NSStringCompareOptions.RegularExpressionSearch, range: nil)
                    //let number = phoneNumber.string
                    //let newString = number.stringByReplacingOccurrencesOfString(" ", withString: "", options: NSStringCompareOptions.LiteralSearch, range: nil)
                    let number1 = number.condenseWhitespace()
                    
                    dic = NSDictionary(objectsAndKeys: contactName, "contactName", number1, "contactPhone")
                }
                else {
                    dic = NSDictionary(object: contactName, forKey: "contactName")
                }
                arr.append(dic)
            }
        }
        
        self.arrContacts = sorted(arr, forwards)
    }
    
    func forwards(d1: NSDictionary, d2: NSDictionary) -> Bool {
        return (d1["contactName"] as String) < (d2["contactName"] as String)
    }
    
    func qsort(input: [String]) -> [String] {
        if let (pivot, rest) = input.decompose {
            let lesser = rest.filter { $0 < pivot }
            let greater = rest.filter { $0 >= pivot }
            return qsort(lesser) + [pivot] + qsort(greater)
        } else {
            return []
        }
    }

    func getFriends() {
        
        if !self.isReachable() {
            return
        }
        
        var defaults = NSUserDefaults.standardUserDefaults();
        var userId = defaults.valueForKeyPath("userData.user_id") as String
        self.view.userInteractionEnabled = false
        self.activityIndicator.startAnimating()
        Alamofire.request(.GET, baseUrl + "viewFriends", parameters:["userID" : userId])
            .responseJSON { (_, _, json, error) -> Void in
                
                if error == nil {
                    var jsonResponse: AnyObject = json!
                    println(jsonResponse)
                    if (jsonResponse["status"] as String == "SUCCESS") {
                        
                        var arrContact = jsonResponse["data"] as Array<NSDictionary>
                        for dic: NSDictionary in arrContact {
                            if var phoneNumber: String = dic["phoneNumber"] as? String {
                                
                                //Ayaz: Remove from number
                                phoneNumber = self.removeCountryCode(phoneNumber)
                                
                                var arr: NSArray = self.arrContacts
                                var predicate: NSPredicate = NSPredicate(format: "contactPhone contains[cd] %@", phoneNumber)!
                                var arrResult = arr.filteredArrayUsingPredicate(predicate) as Array<NSDictionary>
                                
                                if !arrResult.isEmpty {
                                    var friendData: FriendData = FriendData()
                                    friendData.userId = dic["user_id"] as String
                                    friendData.userName = dic["username"] as String
                                    friendData.contactName = arrResult[0]["contactName"] as String
                                    friendData.userNumber = arrResult[0]["contactPhone"] as String
                                    self.arrFriends.append(friendData)
                                }
                            }
                        }
                        self.arrFriendsData = self.arrFriends
                        self._tableView.reloadData()
                    }
                    else {
                        var alert :UIAlertView = UIAlertView(title: "", message: jsonResponse["msg"] as? String, delegate: nil, cancelButtonTitle: "OK")
                        alert.show();
                    }
                }
                self.view.userInteractionEnabled = true
                self.activityIndicator.stopAnimating()
        }
        
    }
    
    func removeCountryCode(phoneNumber:String) -> String {
        var number:NSString = phoneNumber as NSString
        number = number.stringByReplacingOccurrencesOfString("+", withString: "")
        let predicate: NSPredicate = NSPredicate(format: "%@ BEGINSWITH[cd] self", number)!
        var arr = self.countryCodes as NSArray
        let arrResult = arr.filteredArrayUsingPredicate(predicate) as NSArray
        if arrResult.count > 0 {
            number = number.substringFromIndex((arrResult[0] as NSString).length)
        }
        return number as String;
    }
    
//    func getFriends() {
//        
//        if !self.isReachable() {
//            return
//        }
//        
//        var defaults = NSUserDefaults.standardUserDefaults();
//        var userId = defaults.valueForKeyPath("userData.user_id") as String
//        self.view.userInteractionEnabled = false
//        self.activityIndicator.startAnimating()
//        Alamofire.request(.GET, baseUrl + "viewFriends", parameters:["userID" : userId])
//            .responseJSON { (_, _, json, error) -> Void in
//                var jsonResponse: AnyObject = json!
//                println(jsonResponse)
//                if (jsonResponse["status"] as String == "SUCCESS") {
//                    
//                    var arr: Array<NSDictionary> = jsonResponse["data"] as Array<NSDictionary>
//                    
//                    for dic: NSDictionary in arr {
//                        
//                        var friendData: FriendData = FriendData()
//                        friendData.userId = dic["user_id"] as String
//                        friendData.userName = dic["username"] as String
//                        friendData.userNumber = dic["phoneNumber"] as String
//                        self.arrFriends.append(friendData)
//                    }
//                    self.arrFriendsData = self.arrFriends
//                    self._tableView.reloadData()
//                }
//                else {
//                    var alert :UIAlertView = UIAlertView(title: "", message: jsonResponse["msg"] as? String, delegate: nil, cancelButtonTitle: "OK")
//                    alert.show();
//                }
//                self.view.userInteractionEnabled = true
//                self.activityIndicator.stopAnimating()
//        }
//        
//    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return self.arrFriendsData.count;
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        var cell: UITableViewCell = tableView.dequeueReusableCellWithIdentifier("Cell", forIndexPath: indexPath) as UITableViewCell;
        
        //var imageViewBg: UIImageView = cell.contentView.viewWithTag(1000) as UIImageView
        
        var lblName: UILabel = cell.contentView.viewWithTag(1001) as UILabel
        let strUsername = self.arrFriendsData[indexPath.row].userName//self.arrFriends[indexPath.row]["username"] as String
        lblName.text = strUsername
        
        var btnCheckbox: UIButton = cell.contentView.viewWithTag(1002) as UIButton
        btnCheckbox.selected = self.arrFriendsData[indexPath.row].selected
        
        return cell;
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        
        var cell: UITableViewCell = tableView.cellForRowAtIndexPath(indexPath) as UITableViewCell!
        
        var btnCheckbox: UIButton = cell.contentView.viewWithTag(1002) as UIButton
        btnCheckbox.selected = !btnCheckbox.selected
        self.arrFriendsData[indexPath.row].selected = btnCheckbox.selected

    }
    
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        return textField.resignFirstResponder()
    }
    
    func searchTableList() {
        
        self.arrFriendsData.removeAll(keepCapacity: false)
        if textFieldSearch.text.isEmpty {
            self.arrFriendsData = self.arrFriends
        }
        else {
            var arr: NSArray = self.arrFriends
            var predicate: NSPredicate = NSPredicate(format: "userName contains[cd] %@", textFieldSearch.text)!
            self.arrFriendsData = arr.filteredArrayUsingPredicate(predicate) as Array<FriendData>
        }
        
        self.btnDone.enabled = !self.arrFriendsData.isEmpty
        
        self._tableView.reloadData()
    }
    
    func doneBtnPressed(sender: AnyObject) {
        
        var isAnyPerson: Bool = false
        var selectedUserCount: Int = 0;
        
        var userIds: String = ""
        
        for fData:FriendData in self.arrFriendsData {
            if fData.selected {
                selectedUserCount += 1
                if userIds.isEmpty {
                    userIds = fData.userId
                }
                else {
                    userIds += ",\(fData.userId)"
                }
                isAnyPerson = true
            }
        }
        if(selectedUserCount>0){
            if(selectedUserCount == 1){
                self.userMessage = "You just PUSHED your friend"
            }
            else if(selectedUserCount == self.arrFriendsData.count){
                self.userMessage = "You just PUSHED all your friends"
            }
            else{
                self.userMessage = "You just PUSHED your friends"
            }
        }
        
        if !isAnyPerson {
            var alert :UIAlertView = UIAlertView(title: "", message: "Please select atleast one Friend", delegate: nil, cancelButtonTitle: "OK")
            alert.show();
        }
        else {
            if !self.isReachable() {
                return
            }
            //http://ipushnote.com/pushnote/services/index/sendPush?userId=1,6&mesg=Fahad%20is%20saying%20you%20Hello!!!
            
            var defaults = NSUserDefaults.standardUserDefaults();
            var senderName = defaults.valueForKeyPath("userData.username") as String
            
            self.view.userInteractionEnabled = false
            
            self.activityIndicator.startAnimating()
            Alamofire.request(.GET, baseUrl + "sendPush", parameters:["senderName": senderName, "userId" : userIds, "link": self.link, "title": self.titleWeb])
                .responseJSON { (_, _, json, error) -> Void in
                    
                    if error == nil {
                        var jsonResponse: AnyObject = json!
                        if (jsonResponse["status"] as String == "SUCCESS") {
                            var alert :UIAlertView = UIAlertView(title: "", message: self.userMessage, delegate: nil, cancelButtonTitle: "OK")
                            alert.show();
                            self.navigationController?.popViewControllerAnimated(true);
                        }
                        else {
                            var alert :UIAlertView = UIAlertView(title: "", message: jsonResponse["message"] as? String, delegate: nil, cancelButtonTitle: "OK")
                            alert.show();
                        }
                    }
                    self.view.userInteractionEnabled = true
                    self.activityIndicator.stopAnimating()
            }
        }
    }
    
    @IBAction func selectAllBtnPressed(sender: AnyObject) {
        
        isSelectedAll = !isSelectedAll
        for fData:FriendData in self.arrFriendsData {
            fData.selected = isSelectedAll
        }
        self._tableView.reloadData()
    }
    
    @IBAction func checkBoxbtnPressed(sender: AnyObject, forEvent event: UIEvent) {
        
        var touch: UITouch = event.allTouches()?.anyObject() as UITouch
        var point: CGPoint = touch.locationInView(self._tableView)
        var indexPath: NSIndexPath = self._tableView.indexPathForRowAtPoint(point) as NSIndexPath!
        
        var btn: UIButton = sender as UIButton
        
        btn.selected = !btn.selected
        self.arrFriendsData[indexPath.row].selected = btn.selected
        
    }
}
