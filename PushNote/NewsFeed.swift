//
//  NewsFeed.swift
//  PushNote
//
//  Created by Rizwan on 12/11/14.
//  Copyright (c) 2014 com. All rights reserved.
//

import Foundation

class NewsFeed: NSObject {
    
    var feedId: String = ""
    var categoryId: String = ""
    var link: String = ""
    var title: String = ""
    var detail: String = ""
    var userId: String = ""
    var feedPassword: String = ""
    var privacyType: String = ""
    var isSubscribe: Bool = false
}