//
//  ViewController.swift
//  PushNote
//
//  Created by Rizwan on 11/6/14.
//  Copyright (c) 2014 com. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    var homeViewController: HomeViewController!
    var navController: UINavigationController!
    
    var is1st: Bool = true
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        
        if self.is1st {
            NSTimer.scheduledTimerWithTimeInterval(2.0, target: self, selector: "startLogin", userInfo: nil, repeats: false)
            self.is1st = false
        }
        else {
            startLogin()
        }
    }
    
    func startLogin() {
        
        var defaults = NSUserDefaults.standardUserDefaults();
        
        if defaults.objectForKey("userData") != nil {
            navController = self.storyboard?.instantiateViewControllerWithIdentifier("NavC") as UINavigationController
            //navController.modalTransitionStyle = UIModalTransitionStyle.CrossDissolve
            self.presentViewController(navController, animated: true, completion: nil);
        }
        else {
            homeViewController = self.storyboard?.instantiateViewControllerWithIdentifier("HomeViewController") as HomeViewController
            //homeViewController.modalTransitionStyle = UIModalTransitionStyle.CrossDissolve
            self.presentViewController(homeViewController, animated: true, completion: nil);
            
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

