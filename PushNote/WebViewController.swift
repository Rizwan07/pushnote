//
//  WebViewController.swift
//  PushNote
//
//  Created by Rizwan on 12/12/14.
//  Copyright (c) 2014 com. All rights reserved.
//

import UIKit

class WebViewController: BaseViewController {
    
    @IBOutlet var webView: UIWebView!
    
    var link: String? = ""
    var titleWeb: String? = ""
    var isFromNotification: Bool = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        var btnLeft: UIButton = UIButton.buttonWithType(UIButtonType.Custom) as UIButton
        var imgLeft: UIImage! = UIImage(named: "back")
        btnLeft.setImage(imgLeft, forState: UIControlState.Normal)
        btnLeft.frame = CGRectMake(0, 0, 28, 28)
        btnLeft.addTarget(self, action: "backBtnPressed1:", forControlEvents: UIControlEvents.TouchUpInside)
        
        var barLeftBtn: UIBarButtonItem = UIBarButtonItem(customView: btnLeft)
        self.navigationItem.leftBarButtonItem = barLeftBtn
        
        self.navigationItem.title = titleWeb
        if let url = link {
            var request: NSMutableURLRequest = NSMutableURLRequest(URL: NSURL(string: url)!)
            self.webView.loadRequest(request)
        }
    }
    
    func backBtnPressed1(sender: AnyObject) {
        if self.isFromNotification {
            self.navigationController?.view.removeFromSuperview()
        }
        else {
            self.dismissViewControllerAnimated(true, completion: nil)
        }
    }
    
    func webViewDidStartLoad(webView: UIWebView) {
        self.activityIndicator.startAnimating()
    }
    func webViewDidFinishLoad(webView: UIWebView) {
        self.activityIndicator.stopAnimating()
    }
    
    @IBAction func shareMediaBtnPressed(sender: AnyObject) {
        
        var inviteViewController = self.storyboard?.instantiateViewControllerWithIdentifier("InviteFriendsViewController") as UINavigationController
        self.presentViewController(inviteViewController, animated: true, completion: nil);

    }
    
    @IBAction func shareFriendsBtnPressed(sender: AnyObject) {
        
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        
        if segue.identifier == "PushFriends" {
            var destController: ShareFriendsViewController = segue.destinationViewController as ShareFriendsViewController
            destController.link = self.link
            destController.titleWeb = self.titleWeb

        }
        else {
            var destController: ShareViewController = segue.destinationViewController as ShareViewController
            destController.link = self.link
            destController.titleWeb = self.titleWeb

        }
    }
}

