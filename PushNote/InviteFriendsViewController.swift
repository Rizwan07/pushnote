//
//  InviteFriendsViewController.swift
//  PushNote
//
//  Created by Rizwan on 11/6/14.
//  Copyright (c) 2014 com. All rights reserved.
//

import MessageUI
import Social

class InviteFriendsViewController: BaseViewController, MFMessageComposeViewControllerDelegate, MFMailComposeViewControllerDelegate, UINavigationControllerDelegate, UIDocumentInteractionControllerDelegate, GPPSignInDelegate {
    
    @IBOutlet weak var _tableView: UITableView!
    var arrInviteType: Array<String> = []
    let kClientId = "883274197078-iihhi1jj1mu519h5ahut65e3j98smeb5.apps.googleusercontent.com"
    var isIndex: Bool = false
    var _documentInteractionController: UIDocumentInteractionController!
    
    var textToShare: String = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        arrInviteType = ["invite-via-facebook", "invite-via-message", "invite-via-email", "invite-via-whats-app", "invite-via-twitter", "invite-via-google+"]
        
        let userName = NSUserDefaults.standardUserDefaults().valueForKeyPath("userData.username") as String
        self.textToShare = "I am inviting you to Pushnote (https://itunes.apple.com/gb/app/push-note/id962393538?mt=8) Push me back! @\(userName)"
        
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return self.arrInviteType.count;
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        var cell: UITableViewCell = tableView.dequeueReusableCellWithIdentifier("Cell", forIndexPath: indexPath) as UITableViewCell;
        
        var imageViewBg: UIImageView = cell.contentView.viewWithTag(1000) as UIImageView
        imageViewBg.image = UIImage(named: self.arrInviteType[indexPath.row]);
        
        return cell;
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        
        tableView.deselectRowAtIndexPath(indexPath, animated: false)
        
        switch indexPath.row {
            
        case 0:
            if (SLComposeViewController.isAvailableForServiceType(SLServiceTypeFacebook)) {
                
                var controller = SLComposeViewController(forServiceType: SLServiceTypeFacebook)
                controller.setInitialText(self.textToShare)
                controller.addImage(UIImage(named: "Icon"))
                self.presentViewController(controller, animated: true, completion: nil)
            }
            else {
                var alert :UIAlertView = UIAlertView(title: "", message: "Please login with your Facebook account in settings", delegate: nil, cancelButtonTitle: "OK")
                alert.show();
            }
            break
        case 1:
            if (MFMessageComposeViewController.canSendText()) {
                
                let compose: MFMessageComposeViewController = MFMessageComposeViewController()
                compose.messageComposeDelegate = self
//                compose.subject = "Hey! Checkout this cool app called 'Pushnote'. Enjoy!!"
                compose.body = self.textToShare
                self.presentViewController(compose, animated: true, completion: nil)
            }
            break
        case 2:
            if (MFMailComposeViewController.canSendMail()) {
                
                var compose: MFMailComposeViewController = MFMailComposeViewController()
                compose.mailComposeDelegate = self
                compose.setSubject("PushNote Invite")
                compose.setMessageBody(self.textToShare, isHTML: false)
                compose.modalTransitionStyle = UIModalTransitionStyle.CrossDissolve
//                var data: NSData = UIImagePNGRepresentation(UIImage(named: "Icon-29"))
//                compose.addAttachmentData(data, mimeType: "image/png", fileName: "LoveIt.png")
                
                self.presentViewController(compose, animated: true, completion: nil)
            }
            break
        case 3:
            var escapedString = CFURLCreateStringByAddingPercentEscapes(
                nil,
                self.textToShare,
                nil,
                "!*'();:@&=+$,/?%#[]",
                CFStringBuiltInEncodings.UTF8.rawValue
            )

            let urlWhatsApp: String = "whatsapp://send?text=\(escapedString)"
            let url: NSURL = NSURL(string: urlWhatsApp)!
            
            if UIApplication.sharedApplication().canOpenURL(url) {
                UIApplication.sharedApplication().openURL(url)
            }
            else {
                var alert :UIAlertView = UIAlertView(title: "WhatsApp not installed.", message: "Your device has no WhatsApp installed.", delegate: nil, cancelButtonTitle: "OK")
                alert.show();
            }
            break
        case 4:
            if (SLComposeViewController.isAvailableForServiceType(SLServiceTypeTwitter)) {
                
                var controller = SLComposeViewController(forServiceType: SLServiceTypeTwitter)
                controller.setInitialText(self.textToShare)
                controller.addImage(UIImage(named: "Icon"))
                self.presentViewController(controller, animated: true, completion: nil)
            }
            else {
                var alert :UIAlertView = UIAlertView(title: "", message: "Please login with your Twitter account in settings", delegate: nil, cancelButtonTitle: "OK")
                alert.show();
            }
            break
        case 5:
            
            var signIn: GPPSignIn = GPPSignIn.sharedInstance()
            signIn.shouldFetchGooglePlusUser = true
            signIn.shouldFetchGoogleUserEmail = true
            
            signIn.clientID = kClientId
            signIn.scopes = [kGTLAuthScopePlusLogin]
            signIn.delegate = self
            signIn.authenticate()
            break
        default:
            println("nothing selected")
            break
        }
    }
    
    
    func messageComposeViewController(controller: MFMessageComposeViewController!, didFinishWithResult result: MessageComposeResult) {
        
        controller.dismissViewControllerAnimated(true, completion: nil)
    }
    
    func mailComposeController(controller: MFMailComposeViewController!, didFinishWithResult result: MFMailComposeResult, error: NSError!) {
        
        controller.dismissViewControllerAnimated(true, completion: nil)
    }
    
    func finishedWithAuth(auth: GTMOAuth2Authentication!, error: NSError!) {
        
        if error == nil  {
            
            var shareBuilder = GPPShare.sharedInstance().nativeShareDialog()
            
            shareBuilder.setPrefillText(self.textToShare)
            shareBuilder.attachImage(UIImage(named: "Icon.png"))
            shareBuilder.open()
        }
    }
    
}
