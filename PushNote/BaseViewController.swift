//
//  BaseViewController.swift
//  PushNote
//
//  Created by Rizwan on 11/6/14.
//  Copyright (c) 2014 com. All rights reserved.
//

import UIKit

let baseUrl: String = "http://ipushnote.com/pushnote/services/index/"
//let baseUrl: String = "http://loveitbutton.com/services/pushnote/services/index/"

class BaseViewController: UIViewController {
    
    let reachability = Reachability.reachabilityForInternetConnection()
    
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    
    override func viewDidLoad() {
        super.viewDidLoad();
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning();
    }
    
    @IBAction func backBtnPressed (sender: AnyObject) {
        
        if (self.navigationController != nil) {
            if self.navigationController?.viewControllers.count == 3 {
                if self.navigationController?.viewControllers[1].isKindOfClass(AddPhoneViewController) == true {
                    self.navigationController?.popToRootViewControllerAnimated(true)
                }
            }
            self.navigationController?.popViewControllerAnimated(true);
        }
        else {
            self.dismissViewControllerAnimated(true, completion: nil);
        }
    }
    
    func isReachable() -> Bool {
        
        if !self.reachability.isReachable() {
            
            var alert :UIAlertView = UIAlertView(title: "", message: "Please check your Internet Connection", delegate: nil, cancelButtonTitle: "OK")
            alert.show()
            return false
        }
        return true
    }
}
