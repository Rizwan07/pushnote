//
//  AddPhoneViewController.swift
//  PushNote
//
//  Created by Rizwan on 12/23/14.
//  Copyright (c) 2014 com. All rights reserved.
//

import UIKit
import Alamofire

class AddPhoneViewController: BaseViewController, UIAlertViewDelegate {
    
    @IBOutlet weak var textFieldPhoneNumber: UITextField!
    @IBOutlet weak var constraintViewYOffset: NSLayoutConstraint!
    @IBOutlet weak var lblCountryCode: UILabel!
    @IBOutlet weak var pickerView: UIPickerView!
    
    var arrCountryNames: Array<NSString>!
    var arrCountryCodes: Array<NSString>!
    
    var code: Int = 0
    var lastEnteredCode: String = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        arrCountryNames = ["Afghanistan",
            "Albania",
            "Algeria",
            "American Samoa",
            "Andorra",
            "Angola",
            "Anguilla",
            "Antarctica",
            "Antigua and Barbuda",
            "Argentina",
            "Armenia",
            "Aruba",
            "Australia",
            "Austria",
            "Azerbaijan",
            "Bahamas",
            "Bahrain",
            "Bangladesh",
            "Barbados",
            "Belarus",
            "Belgium",
            "Belize",
            "Benin",
            "Bermuda",
            "Bhutan",
            "Bolivia",
            "Bosnia and Herzegovina",
            "Botswana",
            "Brazil",
            //"British Indian Ocean Territory",
            "British Virgin Islands",
            "Brunei",
            "Bulgaria",
            "Burkina Faso",
            "Burma (Myanmar)",
            "Burundi",
            "Cambodia",
            "Cameroon",
            "Canada",
            "Cape Verde",
            "Cayman Islands",
            "Central African Republic",
            "Chad",
            "Chile",
            "China",
            "Christmas Island",
            "Cocos (Keeling) Islands",
            "Colombia",
            "Comoros",
            "Cook Islands",
            "Costa Rica",
            "Croatia",
            "Cuba",
            "Cyprus",
            "Czech Republic",
            "Democratic Republic of the Congo",
            "Denmark",
            "Djibouti",
            "Dominica",
            "Dominican Republic",
            "Ecuador",
            "Egypt",
            "El Salvador",
            "Equatorial Guinea",
            "Eritrea",
            "Estonia",
            "Ethiopia",
            "Falkland Islands",
            "Faroe Islands",
            "Fiji",
            "Finland",
            "France",
            "French Polynesia",
            "Gabon",
            "Gambia",
            "Gaza Strip",
            "Georgia",
            "Germany",
            "Ghana",
            "Gibraltar",
            "Greece",
            "Greenland",
            "Grenada",
            "Guam",
            "Guatemala",
            "Guinea",
            "Guinea-Bissau",
            "Guyana",
            "Haiti",
            "Holy See (Vatican City)",
            "Honduras",
            "Hong Kong",
            "Hungary",
            "Iceland",
            "India",
            "Indonesia",
            "Iran",
            "Iraq",
            "Ireland",
            "Isle of Man",
            "Israel",
            "Italy",
            "Ivory Coast",
            "Jamaica",
            "Japan",
            //"Jersey",
            "Jordan",
            "Kazakhstan",
            "Kenya",
            "Kiribati",
            "Kosovo",
            "Kuwait",
            "Kyrgyzstan",
            "Laos",
            "Latvia",
            "Lebanon",
            "Lesotho",
            "Liberia",
            "Libya",
            "Liechtenstein",
            "Lithuania",
            "Luxembourg",
            "Macau",
            "Macedonia",
            "Madagascar",
            "Malawi",
            "Malaysia",
            "Maldives",
            "Mali",
            "Malta",
            "Marshall Islands",
            "Mauritania",
            "Mauritius",
            "Mayotte",
            "Mexico",
            "Micronesia",
            "Moldova",
            "Monaco",
            "Mongolia",
            "Montenegro",
            "Montserrat",
            "Morocco",
            "Mozambique",
            "Namibia",
            "Nauru",
            "Nepal",
            "Netherlands",
            "Netherlands Antilles",
            "New Caledonia",
            "New Zealand",
            "Nicaragua",
            "Niger",
            "Nigeria",
            "Niue",
            "Norfolk Island",
            "North Korea",
            "Northern Mariana Islands",
            "Norway",
            "Oman",
            "Pakistan",
            "Palau",
            "Panama",
            "Papua New Guinea",
            "Paraguay",
            "Peru",
            "Philippines",
            "Pitcairn Islands",
            "Poland",
            "Portugal",
            "Puerto Rico",
            "Qatar",
            "Republic of the Congo",
            "Romania",
            "Russia",
            "Rwanda",
            "Saint Barthelemy",
            "Saint Helena",
            "Saint Kitts and Nevis",
            "Saint Lucia",
            "Saint Martin",
            "Saint Pierre and Miquelon",
            "Saint Vincent and the Grenadines",
            "Samoa",
            "San Marino",
            "Sao Tome and Principe",
            "Saudi Arabia",
            "Senegal",
            "Serbia",
            "Seychelles",
            "Sierra Leone",
            "Singapore",
            "Slovakia",
            "Slovenia",
            "Solomon Islands",
            "Somalia",
            "South Africa",
            "South Korea",
            "Spain",
            "Sri Lanka",
            "Sudan",
            "Suriname",
            "Swaziland",
            "Sweden",
            "Switzerland",
            "Syria",
            "Taiwan",
            "Tajikistan",
            "Tanzania",
            "Thailand",
            "Timor-Leste",
            "Togo",
            "Tokelau",
            "Tonga",
            "Trinidad and Tobago",
            "Tunisia",
            "Turkey",
            "Turkmenistan",
            "Turks and Caicos Islands",
            "Tuvalu",
            "Uganda",
            "Ukraine",
            "United Arab Emirates",
            "United Kingdom",
            "United States",
            "Uruguay",
            "US Virgin Islands",
            "Uzbekistan",
            "Vanuatu",
            "Venezuela",
            "Vietnam",
            "Wallis and Futuna",
            "West Bank",
            //"Western Sahara",
            "Yemen",
            "Zambia",
            "Zimbabwe"]
        
        self.arrCountryCodes = ["93",
            "355",
            "213",
            "1684",
            "376",
            "244",
            "1264",
            "672",
            "1268",
            "54",
            "374",
            "297",
            "61",
            "43",
            "994",
            "1242",
            "973",
            "880",
            "1246",
            "375",
            "32",
            "501",
            "229",
            "1441",
            "975",
            "591",
            "387",
            "267",
            "55",
            "1284",
            "673",
            "359",
            "226",
            "95",
            "257",
            "855",
            "237",
            "1",
            "238",
            "1345",
            "236",
            "235",
            "56",
            "86",
            "61",
            "61",
            "57",
            "269",
            "682",
            "506",
            "385",
            "53",
            "357",
            "420",
            "243",
            "45",
            "253",
            "1767",
            "1809",
            "593",
            "20",
            "503",
            "240",
            "291",
            "372",
            "251",
            "500",
            "298",
            "679",
            "358",
            "33",
            "689",
            "241",
            "220",
            "970",
            "995",
            "49",
            "233",
            "350",
            "30",
            "299",
            "1473",
            "1671",
            "502",
            "224",
            "245",
            "592",
            "509",
            "39",
            "504",
            "852",
            "36",
            "354",
            "91",
            "62",
            "98",
            "964",
            "353",
            "44",
            "972",
            "39",
            "225",
            "1876",
            "81",
            "962",
            "7",
            "254",
            "686",
            "381",
            "965",
            "996",
            "856",
            "371",
            "961",
            "266",
            "231",
            "218",
            "423",
            "370",
            "352",
            "853",
            "389",
            "261",
            "265",
            "60",
            "960",
            "223",
            "356",
            "692",
            "222",
            "230",
            "262",
            "52",
            "691",
            "373",
            "377",
            "976",
            "382",
            "1664",
            "212",
            "258",
            "264",
            "674",
            "977",
            "31",
            "599",
            "687",
            "64",
            "505",
            "227",
            "234",
            "683",
            "672",
            "850",
            "1670",
            "47",
            "968",
            "92",
            "680",
            "507",
            "675",
            "595",
            "51",
            "63",
            "870",
            "48",
            "351",
            "1",
            "974",
            "242",
            "40",
            "7",
            "250",
            "590",
            "290",
            "1869",
            "1758",
            "1599",
            "508",
            "1784",
            "685",
            "378",
            "239",
            "966",
            "221",
            "381",
            "248",
            "232",
            "65",
            "421",
            "386",
            "677",
            "252",
            "27",
            "82",
            "34",
            "94",
            "249",
            "597",
            "268",
            "46",
            "41",
            "963",
            "886",
            "992",
            "255",
            "66",
            "670",
            "228",
            "690",
            "676",
            "1868",
            "216",
            "90",
            "993",
            "1649",
            "688",
            "256",
            "380",
            "971",
            "44",
            "1",
            "598",
            "1340",
            "998",
            "678",
            "58",
            "84",
            "681",
            "970",
            "967",
            "260",
            "263"]
        
        
    }
    
    func numberOfComponentsInPickerView(pickerView: UIPickerView) -> Int {
        return 1
    }
    
    // returns the # of rows in each component..
    func pickerView(pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        
        return arrCountryNames.count
    }
    
//    func pickerView(pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String! {
//        
//        return arrCountryNames[row]
//    }
    
    func pickerView(pickerView: UIPickerView, attributedTitleForRow row: Int, forComponent component: Int) -> NSAttributedString? {
        let string = arrCountryNames[row]
        return NSAttributedString(string: string, attributes: [NSForegroundColorAttributeName:UIColor.whiteColor()])
    }
    
    func pickerView(pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        
        lblCountryCode.text = "+" + self.arrCountryCodes[row]
    }
    
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        return textField.resignFirstResponder()
    }
    
    @IBAction func countryCodeBtnPressed(sender: AnyObject) {
        textFieldPhoneNumber .resignFirstResponder()
    }
    
    @IBAction func cancelBtnPressed(sender: AnyObject) {
        self.navigationController?.popViewControllerAnimated(true)
    }
    
    @IBAction func goNextBtnPressed(sender: AnyObject) {
        
        if (textFieldPhoneNumber.text.isEmpty) {
            textFieldPhoneNumber.becomeFirstResponder()
            var alert:UIAlertView = UIAlertView(title: "", message: "Please Enter your Phone number", delegate: nil, cancelButtonTitle: "OK")
            alert.show()
            return
        }
        else {
            
            if !self.isReachable() {
                return
            }
            
            self.view.userInteractionEnabled = false
            self.activityIndicator.startAnimating()
            
            Alamofire.request(.GET, baseUrl + "genCode", parameters:["phoneNumber" : lblCountryCode.text! + textFieldPhoneNumber.text])
                .responseJSON { (_, _, json, error) -> Void in
                    
                    if error == nil {
                        var jsonResponse: AnyObject = json!
                        println("jsonresponse \(jsonResponse)")
                        if (jsonResponse["status"] as String == "SUCCESS") {
                            
                            self.code = jsonResponse["code"] as Int
                            self.showAlertWithText("")
//                            var alert :UIAlertView = UIAlertView(title: "", message: "Please enter the Verification Code", delegate: self, cancelButtonTitle: "OK")
//                            alert.alertViewStyle = UIAlertViewStyle.PlainTextInput
//                            alert.textFieldAtIndex(0)?.keyboardType = UIKeyboardType.NumberPad
//                            alert.textFieldAtIndex(0)?.becomeFirstResponder()
//                            alert.tag = 1234
//                            alert.show();
                        }
                        else {
                            var alert :UIAlertView = UIAlertView(title: "", message: jsonResponse["msg"] as? String, delegate: nil, cancelButtonTitle: "OK")
                            alert.show();
                        }
                    }
                    self.view.userInteractionEnabled = true
                    self.activityIndicator.stopAnimating()
            }
        }
        
    }
    
    func showAlertWithText(lastCode: String) {
        var alert = UIAlertController(title: "", message: "Please enter the Verification Code",
            preferredStyle: UIAlertControllerStyle.Alert)
        
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.Cancel, handler: { (alertAction: UIAlertAction!) -> Void in
            var textField: UITextField = alert.textFields?.first as UITextField
            self.lastEnteredCode = textField.text
            if !textField.text.isEmpty {
                
                if self.code == textField.text.toInt() {
                    self.addMyPhoneNumber()
                }
                else {
                    var alert :UIAlertView = UIAlertView(title: "", message: "Wrong Code", delegate: self, cancelButtonTitle: "Cancel", otherButtonTitles: "Re-Enter")
                    alert.tag = 4321
                    alert.show()
                    
                }
            }
            else {
                var alert :UIAlertView = UIAlertView(title: "", message: "Wrong Code", delegate: self, cancelButtonTitle: "Cancel", otherButtonTitles: "Re-Enter")
                alert.tag = 4321
                alert.show()
                
            }

        }))
        
        alert.addTextFieldWithConfigurationHandler({(textField: UITextField!) in
            textField.keyboardType = UIKeyboardType.NumberPad
        })
        self.presentViewController(alert, animated: true, completion: nil)
    }
    
    func addMyPhoneNumber() {
        
        if !self.isReachable() {
            return
        }
        
        self.view.userInteractionEnabled = false
        self.activityIndicator.startAnimating()
        
        var defaults = NSUserDefaults.standardUserDefaults();
        var userId = defaults.valueForKeyPath("userData.user_id") as String
        var arrParam = ["userId" : userId]
        arrParam["phone"] = lblCountryCode.text! + textFieldPhoneNumber.text
        
        Alamofire.request(.GET, baseUrl + "addPhone", parameters:arrParam)
            .responseJSON { (_, _, json, error) -> Void in
                
                if error == nil {
                    var jsonResponse: AnyObject = json!
                    println("jsonresponse \(jsonResponse)")
                    if (jsonResponse["status"] as String == "SUCCESS") {
                        
                        var defaults = NSUserDefaults.standardUserDefaults();
                        defaults.setObject(jsonResponse.valueForKey("data"), forKey: "userData");
                        defaults.synchronize();
                        
                        var findFriendViewController = self.storyboard?.instantiateViewControllerWithIdentifier("FindFriendsViewController") as FindFriendsViewController
                        self.navigationController?.pushViewController(findFriendViewController, animated: true)
                        
                    }
                    else {
                        var alert :UIAlertView = UIAlertView(title: "", message: jsonResponse["msg"] as? String, delegate: nil, cancelButtonTitle: "OK")
                        alert.show();
                    }
                }
                self.view.userInteractionEnabled = true
                self.activityIndicator.stopAnimating()
        }

    }
    
    func alertView(alertView: UIAlertView, clickedButtonAtIndex buttonIndex: Int) {
        
        if alertView.tag == 4321 {
            if buttonIndex == 1 {
                
                self.showAlertWithText(self.lastEnteredCode)
            }
        }
    }

}
