//
//  ShareViewController.swift
//  PushNote
//
//  Created by Rizwan on 1/7/15.
//  Copyright (c) 2015 com. All rights reserved.
//

import UIKit
import Social
import MessageUI

//import "OAuthLoginView.h"
//import "JSONKit.h"
//import "OAConsumer.h"
//import "OAMutableURLRequest.h"
//import "OADataFetcher.h"
//import "OATokenManager.h"

class ShareViewController: BaseViewController, MFMessageComposeViewControllerDelegate, MFMailComposeViewControllerDelegate, UINavigationControllerDelegate, UIDocumentInteractionControllerDelegate, GPPSignInDelegate {
    
    @IBOutlet weak var _tableView: UITableView!
    var arrShareType: Array<String> = []
    let kClientId = "883274197078-iihhi1jj1mu519h5ahut65e3j98smeb5.apps.googleusercontent.com"
    var isIndex: Bool = false
    var _documentInteractionController: UIDocumentInteractionController!
    
    var link: String!
    var titleWeb: String!
    
    var oAuthLoginView: OAuthLoginView!
    var nacController: UINavigationController!

    var textToShare: String = ""
    
    var isShared: Bool = false
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        arrShareType = ["share-via-facebook", "share-via-message", "share-via-email", "share-via-whatsapp", "share-via-twitter", "share-via-google-+", "share-via-linkedin"]
        
        if self.isIndex {
            
            var btnLeft: UIButton = UIButton.buttonWithType(UIButtonType.Custom) as UIButton
            var imgLeft: UIImage! = UIImage(named: "back")
            btnLeft.setImage(imgLeft, forState: UIControlState.Normal)
            btnLeft.frame = CGRectMake(0, 0, 28, 28)
            btnLeft.addTarget(self, action: "backBtnPressed:", forControlEvents: UIControlEvents.TouchUpInside)
            
            var barLeftBtn: UIBarButtonItem = UIBarButtonItem(customView: btnLeft)
            self.navigationItem.leftBarButtonItem = barLeftBtn
            
            self.navigationItem.title = "SHARE"
        }
        
        self.textToShare = "Just got a Pushnote from \(link). Subscribe here https://itunes.apple.com/gb/app/push-note/id962393538?mt=8"
        
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return self.arrShareType.count;
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        var cell: UITableViewCell = tableView.dequeueReusableCellWithIdentifier("Cell", forIndexPath: indexPath) as UITableViewCell;
        
        var imageViewBg: UIImageView = cell.contentView.viewWithTag(1000) as UIImageView
        imageViewBg.image = UIImage(named: self.arrShareType[indexPath.row]);
        
        return cell;
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        
        tableView.deselectRowAtIndexPath(indexPath, animated: true)
        
        switch indexPath.row {
            
        case 0:
            if (SLComposeViewController.isAvailableForServiceType(SLServiceTypeFacebook)) {
                
                var controller = SLComposeViewController(forServiceType: SLServiceTypeFacebook)
                controller.setInitialText(self.textToShare)
                controller.addImage(UIImage(named: "Icon"))
                self.presentViewController(controller, animated: true, completion: nil)
            }
            else {
                var alert :UIAlertView = UIAlertView(title: "", message: "Please login with your Facebook account in settings", delegate: nil, cancelButtonTitle: "OK")
                alert.show();
            }
            break
        case 1:
            if (MFMessageComposeViewController.canSendText()) {
                
                var compose: MFMessageComposeViewController = MFMessageComposeViewController()
                compose.messageComposeDelegate = self
                //compose.subject = self.textToShare
                compose.body = self.textToShare
                self.presentViewController(compose, animated: true, completion: nil)
            }
            break
        case 2:
            if (MFMailComposeViewController.canSendMail()) {
                
                var compose: MFMailComposeViewController = MFMailComposeViewController()
                compose.mailComposeDelegate = self
                compose.setSubject("PushNote Share")
                compose.setMessageBody(self.textToShare, isHTML: false)
                compose.modalTransitionStyle = UIModalTransitionStyle.CrossDissolve
//                var data: NSData = UIImagePNGRepresentation(UIImage(named: "Icon-29"))
//                compose.addAttachmentData(data, mimeType: "image/png", fileName: "LoveIt.png")
                
                self.presentViewController(compose, animated: true, completion: nil)
            }
            break
        case 3:
            
            var escapedString = CFURLCreateStringByAddingPercentEscapes(
                nil,
                self.textToShare,
                nil,
                "!*'();:@&=+$,/?%#[]",
                CFStringBuiltInEncodings.UTF8.rawValue
            )
            
            let urlWhatsApp: String = "whatsapp://send?text=\(escapedString)"
            let url: NSURL = NSURL(string: urlWhatsApp)!
            
            if UIApplication.sharedApplication().canOpenURL(url) {
                UIApplication.sharedApplication().openURL(url)
            }
            else {
                var alert :UIAlertView = UIAlertView(title: "WhatsApp not installed.", message: "Your device has no WhatsApp installed.", delegate: nil, cancelButtonTitle: "OK")
                alert.show();
            }
            
//            if (UIApplication.sharedApplication().canOpenURL(NSURL(string: "whatsapp://app")!)) {
//                
//                var iconImage: UIImage = UIImage(named: "Icon.png")!
//                var savePath: String = NSHomeDirectory().stringByAppendingPathComponent("Documents/whatsAppTmp.wai")
//                UIImageJPEGRepresentation(iconImage, 0.5).writeToFile(savePath, atomically: true)
//                
//                _documentInteractionController = UIDocumentInteractionController(URL: NSURL(fileURLWithPath: savePath)!)
//                _documentInteractionController.UTI = "net.whatsapp.image"
//                _documentInteractionController.delegate = self
//                _documentInteractionController.presentOpenInMenuFromRect(CGRect(x: 0, y: 0, width: 0, height: 0), inView: self.view, animated: true)
//                
//                
//            } else {
//                var alert :UIAlertView = UIAlertView(title: "WhatsApp not installed.", message: "Your device has no WhatsApp installed.", delegate: nil, cancelButtonTitle: "OK")
//                alert.show();
//            }
            break
        case 4:
            if (SLComposeViewController.isAvailableForServiceType(SLServiceTypeTwitter)) {
                
                var controller = SLComposeViewController(forServiceType: SLServiceTypeTwitter)
                controller.setInitialText(self.textToShare)
                controller.addImage(UIImage(named: "Icon"))
                self.presentViewController(controller, animated: true, completion: nil)
            }
            else {
                var alert :UIAlertView = UIAlertView(title: "", message: "Please login with your Twitter account in settings", delegate: nil, cancelButtonTitle: "OK")
                alert.show();
            }
            break
        case 5:
            
            var signIn: GPPSignIn = GPPSignIn.sharedInstance()
            signIn.shouldFetchGooglePlusUser = true
            signIn.shouldFetchGoogleUserEmail = true
            
            signIn.clientID = kClientId
            signIn.scopes = [kGTLAuthScopePlusLogin]
            signIn.delegate = self
            signIn.authenticate()
            break
        case 6:
            
            var tokenKey: NSString = "";
            
            NSUserDefaults.standardUserDefaults().setObject(tokenKey, forKey: "TokenKey")
            NSUserDefaults.standardUserDefaults().synchronize()
            
            self.isShared = false
            
            self.oAuthLoginView = OAuthLoginView(nibName: nil, bundle: nil)
            nacController = UINavigationController(rootViewController: self.oAuthLoginView)
            nacController.navigationBar.barTintColor = UIColor(red: 255.0/255.0, green: 127.0/255.0, blue: 0, alpha: 1.0)
            nacController.navigationBar.translucent = false
            
            // register to be told when the login is finished
//            if self.oAuthLoginView.accessToken.isValid() {
//                profileApiCall()
//            }
//            else {
            
                NSNotificationCenter.defaultCenter().addObserver(self, selector: "loginViewDidFinish:", name: "loginViewDidFinish", object: self.oAuthLoginView)
                
                self.presentViewController(nacController, animated: true, completion: nil)
//            }
            break
        default:
            println("nothing selected")
            break
        }
    }
    
    func messageComposeViewController(controller: MFMessageComposeViewController!, didFinishWithResult result: MessageComposeResult) {
        
        controller.dismissViewControllerAnimated(true, completion: nil)
    }
    
    func mailComposeController(controller: MFMailComposeViewController!, didFinishWithResult result: MFMailComposeResult, error: NSError!) {
        
        controller.dismissViewControllerAnimated(true, completion: nil)
    }
    
    func finishedWithAuth(auth: GTMOAuth2Authentication!, error: NSError!) {
        
        if error == nil  {
            
            var shareBuilder = GPPShare.sharedInstance().nativeShareDialog()
            
            shareBuilder.setPrefillText(self.textToShare)
            shareBuilder.attachImage(UIImage(named: "Icon.png"))
            shareBuilder.open()
        }
        
    }
    
    
    func loginViewDidFinish(notification:NSNotification) {
        
        NSNotificationCenter.defaultCenter().removeObserver(self)
    
    // We're going to do these calls serially just for easy code reading.
    // They can be done asynchronously
    // Get the profile, then the network updates
        profileApiCall()
    }
    
    func profileApiCall() {
        
        var url: NSURL = NSURL(string: "http://api.linkedin.com/v1/people/~")!
        
        var request: OAMutableURLRequest = OAMutableURLRequest(URL: url, consumer: self.oAuthLoginView.consumer, token: self.oAuthLoginView.accessToken, callback: nil, signatureProvider: nil)
        request.setValue("json", forHTTPHeaderField: "x-li-format")
        //request.setValue("json", for: "x-li-format")
        
        var fetcher: OADataFetcher = OADataFetcher()
        fetcher.fetchDataWithRequest(request,
            delegate: self,
            didFinishSelector: "profileApiCallResult:didFinish:",
            didFailSelector: "profileApiCallResult:didFail:")
        
    }

    func profileApiCallResult(ticket: OAServiceTicket, didFinish data:NSData) {
        
        var responseBody: NSString = NSString(data: data, encoding: NSUTF8StringEncoding)!
        var profile: NSDictionary = responseBody.objectFromJSONString() as NSDictionary
        
        if profile.count != 0 {
//            [self.buttonLogin setHidden:YES];
//            
//            self.userName.text = [[NSString alloc] initWithFormat:@"%@ %@",
//            [profile objectForKey:@"firstName"], [profile objectForKey:@"lastName"]];
//            
//            self.headline.text = [profile objectForKey:@"headline"];
            println(profile)
        }
    // The next thing we want to do is call the network updates
        networkApiCall()
    
    }
    
    func profileApiCallResult(ticket:OAServiceTicket, didFail error:NSData) {
        
        NSLog(error.description);
    }

    func networkApiCall() {
        
        var url:NSURL = NSURL(string: "http://api.linkedin.com/v1/people/~/network/updates?scope=self&count=1&type=STAT")!
        
        var request: OAMutableURLRequest = OAMutableURLRequest(URL: url, consumer: self.oAuthLoginView.consumer, token: self.oAuthLoginView.accessToken, callback: nil, signatureProvider: nil)
        request.setValue("json", forHTTPHeaderField: "x-li-format")
        
        var fetcher: OADataFetcher = OADataFetcher()
        fetcher.fetchDataWithRequest(request,
            delegate: self,
            didFinishSelector: "networkApiCallResult:didFinish:",
            didFailSelector: "networkApiCallResult:didFail:")
        
    }
    
    
    func networkApiCallResult(ticket:OAServiceTicket, didFinish data:NSData) {
        
        var responseBody: NSString = NSString(data: data, encoding: NSUTF8StringEncoding)!
        
        var personData: NSDictionary = responseBody.objectFromJSONString() as NSDictionary
        
        //var person: NSDictionary = personData["values"][0]["updateContent"]["person"] as NSDictionary
        //var person: NSDictionary = personData.objectForKey("values")?.objectAtIndex(0).objectForKey("updateContent")?.objectForKey("person") as NSDictionary
        
        if !self.isShared {
            postStatus()
        }
        
//    NSDictionary *person = [[[[[responseBody objectFromJSONString]
//    objectForKey:@"values"]
//    objectAtIndex:0]
//    objectForKey:@"updateContent"]
//    objectForKey:@"person"];
//
        //self.dismissViewControllerAnimated(true, completion: nil)
    }
    
    func networkApiCallResult(ticket:OAServiceTicket, didFail error:NSData) {
        NSLog(error.description);
    }
    
    func postStatus() {
        
        var url: NSURL = NSURL(string: "http://api.linkedin.com/v1/people/~/shares")!
        
        var request: OAMutableURLRequest = OAMutableURLRequest(URL: url, consumer: self.oAuthLoginView.consumer, token: self.oAuthLoginView.accessToken, callback: nil, signatureProvider: nil)
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        
        var comment: String = self.titleWeb + " " + self.link
        
        var update: NSDictionary = NSDictionary(objectsAndKeys: NSDictionary(objectsAndKeys: "anyone","code"),"visibility", comment, "comment" )
        
        var updateString: NSString = update.JSONString()
        
        request.setHTTPBodyWithString(updateString)
        request.HTTPMethod = "POST"
        //request.setHTTPMethod("POST")
        
        var fetcher: OADataFetcher = OADataFetcher()
        fetcher.fetchDataWithRequest(request,
            delegate: self,
            didFinishSelector: "postUpdateApiCallResult:didFinish:",
            didFailSelector: "postUpdateApiCallResult:didFail:")
        
    }
    
    func postUpdateApiCallResult(ticket:OAServiceTicket, didFinish data:NSData) {
    // The next thing we want to do is call the network updates
        
        self.isShared = true
        networkApiCall()
        println(NSString(data: data, encoding: NSUTF8StringEncoding))
        var alert :UIAlertView = UIAlertView(title: "", message: "Posted on Linedin", delegate: nil, cancelButtonTitle: "OK")
        alert.show();

    }

    func postUpdateApiCallResult(ticket:OAServiceTicket, didFail error:NSData) {
        NSLog(error.description);
    }

    
//    #pragma Marks - Public Methods ::
//    
//
//    - (IBAction)buttonLogoutClick:(id)sender
//    {
//    
//    [self.buttonLogin setHidden:NO];
//    
//    NSString *tokenKey = @"";
//    
//    [[NSUserDefaults standardUserDefaults] setObject:tokenKey forKey:@"TokenKey"];
//    
//    
//    [self.navigationController popViewControllerAnimated:YES];
//    UIAlertView *logoutMessageAlert =[[UIAlertView alloc]initWithTitle:@"Alert" message:@"Logout Sucessfully !" delegate:self cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
//    [logoutMessageAlert show];
//    
//    }
//    
//    #pragma Marks - topNavDelagate::
//    
//    -(void)buttonBackClick
//    {
//    [self.navigationController popViewControllerAnimated:YES];
//    }

    
    
}
